
package br.com.estapar.posdigital.model.sync.filial.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tarifa {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tempo")
    @Expose
    private Integer tempo;
    @SerializedName("valor")
    @Expose
    private Double valor;
    @SerializedName("tipoVeiculo")
    @Expose
    private Integer tipoVeiculo;
    @SerializedName("setor")
    @Expose
    private String setor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTempo() {
        return tempo;
    }

    public void setTempo(Integer tempo) {
        this.tempo = tempo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(Integer tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

}
