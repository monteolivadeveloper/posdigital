package br.com.estapar.posdigital.model.printer;

import androidx.annotation.NonNull;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.bean.PrintBean;
import br.com.estapar.posdigital.listener.OnCallbackListener;
import br.com.estapar.posdigital.utils.Utils;

public class PrintTicketSaoPaulo implements OnCallbackListener {
    private PrintModel printer;
    private PosDigitalApplication app;
    private OnCallbackListener listener;

    /**
     * Constructor
     */
    public PrintTicketSaoPaulo(PosDigitalApplication app) {
        this.app     = app;
        this.printer = new PrintModel();
        this.printer.setOnCallbackListener(this);
    }

    public void print(@NonNull PrintBean bean) {
        String dateNow = Utils.getNow();

        printer.addTitle("ESTAPAR");
        printer.addSubTitle("Estacionamentos");
        printer.addLines(1);

        printer.addTextCenter("ATENCAO");
        printer.addTextCenter("VERIFIQUE SUA PLACA");
        printer.addTextCenter("FIQUE ATENTO");
        printer.addTextCenter("AO VENCIMENTO");
        printer.addTextCenter("NAO HA TOLERANCIA");
        printer.addLines(1);

        printer.addTitle("SAO PAULO/SP");
        printer.addTextLeft("");

        printer.addTitle("TIQUETE AVULSO");
        printer.addTextLeft("");

        printer.addTextCenter("COMPROVANTE DE PAGAMENTO");
        printer.addLines(1);

        printer.addTextLeft("No. SERIE: " + app.getInfo().getNrSerie());
        printer.addTextLeft("AUT.: 52367444520190070310475931");
        printer.addTextLeft("VAGA: 1");
        printer.addTextLeft("AREA: 51");
        printer.addTextLeft("SETOR: 20");

        printer.addLines(1);

        printer.addTextLeft("Placa do Veiculo:");
        printer.addTitle(bean.getNrPlaca());
        printer.addTextLeft("");

        printer.addTextLeft("Tarifa: " + bean.getTarifa());
        printer.addTextLeft("Emitido em " + dateNow);
        printer.addTextLeft("Valor pago: R$ " + bean.getValue());
        printer.addTextLeft("Inicio: " + dateNow);
        printer.addTextLeft("Valido ate: " + Utils.getAddMinutes(bean.getMinutes()));
        printer.addLines(1);

        printer.addTextLeft("Forma de Pagamento: " + bean.getFormaPagto());
        printer.addLines(1);
        printer.print();
    }

    public void setOnCallbackListener(OnCallbackListener listener) { this.listener = listener; }

    @Override
    public void onCallbackSuccess(@NonNull int type) {
        if (listener != null) { listener.onCallbackSuccess(type); }
    }

    @Override
    public void onCallbackError(@NonNull String message) {}
}