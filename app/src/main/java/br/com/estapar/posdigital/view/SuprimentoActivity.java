package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.view.KeyEvent;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.model.sync.cashier.CashierModel;
import br.com.estapar.posdigital.model.sync.cashier.listener.OnCashierListener;
import br.com.estapar.posdigital.model.mifare.MifareUtils;
import br.com.estapar.posdigital.utils.Utils;
import br.com.estapar.posdigital.view.components.CurrencyEdit;

@EActivity(R.layout.activity_suprimento)
@Fullscreen
public class SuprimentoActivity extends DefaultActivity {
    private CashierModel model;

    @ViewById(R.id.editSupValue)
    protected CurrencyEdit editValue;
    @ViewById(R.id.txtSupBalance)
    protected TextView textBalance;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        model = new CashierModel.Builder()
                .context(this)
                .application(app)
                .listener(cashierListener)
                .build();
    }

    @Click(R.id.btnSupSave)
    protected void save() {
        msgConfirm(R.string.suprimento_confirm, () -> actSave());
    }

    private void actSave() {
        progress();
        double saveValue = MifareUtils.getDoubleValue(editValue.getValue());
        model.suprimento(saveValue);
    }

    private String showBalance() {
        double saldo = app.getCaixaResponse().getCaixa().getSaldo();
        String valueBalance = Utils.setFormatDecimal(saldo);
        String txtBalance = getString(R.string.txt_balance) + " " + valueBalance;
        textBalance.setText(txtBalance);

        return valueBalance;
    }

    private void back() {
        startActivity(new Intent(this, CashierActivity_.class));
        finish();
    }

    private void menu() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    @Override
    protected void resume() { showBalance(); }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { menu(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /*****************************************************************************
     * CallBack (listener)
     *****************************************************************************/
    private OnCashierListener cashierListener = new OnCashierListener() {
        @Override
        public void openSuccess() {
            String txtBalance = showBalance();
            String message    = getString(R.string.suprimento_success) + "\n\n";
            message          += getString(R.string.txt_new_balance) + " " + txtBalance;

            msgSuccess(message, () -> back());
        }

        @Override
        public void openError(String message) { msgError(message); }

        @Override
        public void openCashierPage() {}
    };
}