package br.com.estapar.posdigital.model;

import android.os.RemoteException;

import androidx.annotation.NonNull;

import com.getnet.posdigital.PosDigital;
import com.getnet.posdigital.info.IInfoCallback;
import com.getnet.posdigital.info.InfoResponse;

import br.com.estapar.posdigital.bean.InfoBean;
import br.com.estapar.posdigital.listener.OnInfoListener;

public class InfoModel {
    public void getInfo(@NonNull OnInfoListener listener) {
        try {
            PosDigital.getInstance().getInfo().info(new IInfoCallback.Stub() {
                @Override
                public void onInfo(InfoResponse infoResponse) {
                    InfoBean infoBean = new InfoBean();
                    infoBean.setNrSerie(infoResponse.getSerialNumber());

                    listener.onInfoLoad(infoBean);
                }

                @Override
                public void onError(String s) {
                }
            });
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}