package br.com.estapar.posdigital.model.sync.user;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import br.com.estapar.posdigital.model.sync.SyncType;
import br.com.estapar.posdigital.model.sync.listener.OnSyncListener;
import br.com.estapar.posdigital.model.sync.user.gson.UsuarioResponse;
import br.com.estapar.posdigital.utils.Utils;
import httpaction.HttpResponse;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.utils.http.RestCallback;

public class UserModel {
    private Context context;
    private OnSyncListener listener;
    private PosDigitalApplication app;
    private UserConfig config;

    UserModel(@NonNull Context context,
              @NonNull PosDigitalApplication app,
              @NonNull OnSyncListener listener) {
        this.context   = context;
        this.app       = app;
        this.listener  = listener;

        config = new UserConfig(context);
        config.setCallBack(callBack);
    }

    private void authenticate(HttpResponse response) { setResponse(response); }
    private void consult(HttpResponse response)      { setResponse(response); }

    private void setResponse(HttpResponse response) {
        UsuarioResponse login = new Gson().fromJson(response.getMessage(), UsuarioResponse.class);
        if (listener != null) {
            int codeHttp = response.getCodeHttp();
            if (codeHttp == 200 || codeHttp == 201) {
                String token = login.getUsuario().getAcessoUsuario().getToken();
                String user  = login.getUsuario().getNome();

                app.setUsuarioResponse(login);

                // persistent token
                Utils.gravaConfig(context, "login_token", token);
                Utils.gravaConfig(context, "login_login", user);

                listener.syncSuccess();
            }
            else { listener.syncError(login.getDescricaoRetorno()); }
        }
    }

    /**************************************************************************************
     * Callback
     **************************************************************************************/
    private RestCallback callBack = new RestCallback() {
        @Override
        public void onResponse(HttpResponse response, int type) {
            switch (type) {
                case SyncType.USER_AUTENTICAR:
                    authenticate(response);
                    break;
                case SyncType.USER_CONSULT:
                    consult(response);
                    break;
            }
        }

        @Override
        public void onError(HttpResponse httpResponse) {
            if (listener != null) { listener.syncError(httpResponse.getMessage()); }
        }
    };

    /**************************************************************************************
     * Actions
     **************************************************************************************/
    public void autenticarUsuario(@NonNull String userLogin, @NonNull String passLogin) {
        this.config.authenticate(userLogin, app.IMEI, passLogin);
    }
    public void consultarUsuario() {
        String token = Utils.getStringConfig(context, "login_token");
        String login = Utils.getStringConfig(context, "login_login");
        this.config.consult(login, app.IMEI, token);
    }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;
        private PosDigitalApplication app;
        private OnSyncListener listener;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder application(PosDigitalApplication app) {
            this.app = app;
            return this;
        }

        public Builder listener(OnSyncListener listener) {
            this.listener = listener;
            return this;
        }

        public UserModel build() {
            if (app == null) {
                throw new IllegalStateException("Application required.");
            }
            else if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            else if (listener == null) {
                throw new IllegalStateException("Listener required.");
            }
            return new UserModel(context, app, listener);
        }
    }
}