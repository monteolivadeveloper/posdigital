package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.os.Handler;
import android.view.KeyEvent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.model.sync.cashier.listener.OnCashierListener;
import br.com.estapar.posdigital.model.sync.cashier.CashierModel;
import br.com.estapar.posdigital.model.sync.filial.FilialModel;
import br.com.estapar.posdigital.model.sync.listener.OnSyncListener;
import br.com.estapar.posdigital.utils.Constantes;

@EActivity(R.layout.activity_main)
@Fullscreen
public class MainActivity extends DefaultActivity {
    private CashierModel cashierModel;
    private FilialModel filialModel;

    @App
    protected PosDigitalApplication app;

    private int MENU_TYPE = 0;

    @AfterViews
    protected void afterViews() {
        cashierModel = new CashierModel.Builder()
                .context(this)
                .application(app)
                .listener(cashierListener)
                .build();

        filialModel = new FilialModel.Builder()
                .context(this)
                .application(app)
                .listener(syncListener)
                .build();
    }

    @Click(R.id.menu01)
    protected void btnVendaTiquete() {
        MENU_TYPE = Constantes.MENU_HOME_TICKET;
        cashierModel.payment();
    }

    @Click(R.id.menu02)
    protected void btnCashier() {
        startActivity(new Intent(getBaseContext(), CashierActivity_.class));
        finish();
    }

    @Click(R.id.menu03)
    protected void btnMenu03() {
    }

    /**
     * EXIT
     */
    @Click(R.id.menu04)
    protected void btnExit() { logout(); }

    /**
     * Recarga (Cartão NFC)
     */
    @Click(R.id.menu05)
    protected void btnRecarga() {
        MENU_TYPE = Constantes.MENU_HOME_MIFARE;
        cashierModel.payment();
    }

    @Click(R.id.menu06)
    protected void btnRegularizacao() {
        MENU_TYPE = Constantes.MENU_HOME_REGULARIZACAO;
        cashierModel.payment();
    }

    private void logout() {
        msgConfirm(R.string.app_exit, R.string.btn_exit, () -> finish());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { logout(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /*****************************************************************************
     * CallBack (listener)
     *****************************************************************************/
    private OnCashierListener cashierListener = new OnCashierListener() {
        @Override
        public void openSuccess() {
            switch (MENU_TYPE) {
                case Constantes.MENU_HOME_TICKET:
                    startActivity(new Intent(getBaseContext(), SaleActivity_.class));
                    finish();
                    break;
                case Constantes.MENU_HOME_MIFARE:
                    startActivity(new Intent(getBaseContext(), MifareActivity_.class));
                    finish();
                    break;
                case Constantes.MENU_CASHIER_CONSULT:
                    filialModel.obterTarifasFilial();
                    break;
                case Constantes.MENU_HOME_REGULARIZACAO:
                    break;
            }
        }

        @Override
        public void openError(String message) {}

        @Override
        public void openCashierPage() {
            startActivity(new Intent(getBaseContext(), CashierOpenActivity_.class));
            finish();
        }
    };

    private OnSyncListener syncListener = new OnSyncListener() {
        @Override
        public void syncSuccess() { hideMsg(); }

        @Override
        public void syncError(String message) { msgError(message); }
    };

    @Override
    protected void resume() {
        new Handler().postDelayed(() -> {
            progress(R.string.progress_sync);
            MENU_TYPE = Constantes.MENU_CASHIER_CONSULT;
            cashierModel.consultarCaixa();
        }, 300);
    }
}