package br.com.estapar.posdigital.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.estapar.posdigital.R;

public class Utils {

    /**
     * Metodo que retorna zeros a esquerda
     *
     * @param campo
     * @param qtde
     * @return
     */
    public static String strZero(String campo, int qtde) {
        // seta a quantidade de zeros
        final int zeros = qtde - campo.length();

        // seta o retorno
        String retorno  = "";

        // gera na variavel final o numero de zeros
        for (int ii = 0; ii < zeros; ii++) { retorno += "0"; }

        // retorna
        return retorno + campo;
    }
    public static String strSpace(String campo, int qtde) {
        // seta a quantidade de zeros
        final int zeros = qtde - campo.length();

        // seta o retorno
        String retorno  = "";

        // gera na variavel final o numero de zeros
        for (int ii = 0; ii < zeros; ii++) { retorno += " "; }

        // retorna
        return campo + retorno;
    }


    public static void hideNavigationBar(AppCompatActivity activity) {
        View decorView = activity.getWindow().getDecorView();
             decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public static void msgBox(@NonNull Context context, @NonNull int message) {
        msgBox(context, context.getString(message));
    }
    public static void msgBox(@NonNull Context context, @NonNull String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        dialog.setCancelable(false);
        dialog.setMessage(message);
        dialog.setNegativeButton(
                context.getString(R.string.btn_ok),
                (dialogInterface, i) -> dialogInterface.dismiss());
        dialog.create().show();
    }

    public static String getNow() {
        // seta a Formatacao
        final Locale locale = new Locale("pt", "BR");
        final SimpleDateFormat formatacao = new SimpleDateFormat("dd/MM/yyyy HH:mm", locale);

        // instancia o calendario
        final GregorianCalendar mCalendario = new GregorianCalendar(locale);
                                mCalendario.setTimeInMillis(System.currentTimeMillis());

        // seta a data final
        String data = formatacao.format(mCalendario.getTime());

        // retorna a Data e Hora de gravacao
        return data;
    }

    public static String getAddMinutes(int minutes) {
        // seta a Formatacao
        final Locale locale = new Locale("pt", "BR");
        final SimpleDateFormat formatacao = new SimpleDateFormat("dd/MM/yyyy HH:mm", locale);

        // instancia o calendario
        final GregorianCalendar mCalendario = new GregorianCalendar(locale);
                                mCalendario.setTimeInMillis(System.currentTimeMillis());
                               mCalendario.add(Calendar.MINUTE, minutes);

        // seta a data final
        final String data = formatacao.format(mCalendario.getTime());

        // retorna a Data e Hora de gravacao
        return data;

    }

    public static String setFormatDecimal(double valor) {
        // pega o formato
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(new Locale("pt", "BR"));
        DecimalFormat fDecimal       = new DecimalFormat("###,##0.00", symbols);

        // retorna o valor formatado
        return fDecimal.format(valor);
    }
    public static String setFormatDecimal(float valor) {
        // pega o formato
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(new Locale("pt", "BR"));
        DecimalFormat fDecimal       = new DecimalFormat("###,##0.00", symbols);

        // retorna o valor formatado
        return fDecimal.format(valor);
    }

    /**
     * Metodo que retorna um CPF formatado
     *
     * @param number
     * @return
     */
    public static String setFormatCPF(String number) {
        Pattern pattern = Pattern.compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
        Matcher matcher = pattern.matcher(number);
        if (matcher.matches()) {
            number = matcher.replaceAll("$1.$2.$3-$4");
        }
        return number;
    }

    /**
     * Metodo que grava na Preferences a chave (String)
     */
    public static void gravaConfig(Context contexto, String chave, String valor) {
        SharedPreferences settings = contexto.getSharedPreferences("config", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(chave, valor);
        editor.commit();
    }

    /**
     * Metodo que pega a chave do Preferences
     *
     * @return
     */
    public static String getStringConfig(Context contexto, String chave) {
        // seta o retorno
        String retorno = "";

        try {
            retorno = (contexto.getSharedPreferences("config", Context.MODE_PRIVATE)).getString(chave, "");
        } catch (Exception ex) {
        }

        // retorna
        return retorno;
    }

    public static void gravaConfig(Context contexto, String chave, int valor) {
        SharedPreferences settings = contexto.getSharedPreferences("config", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(chave, valor);
        editor.commit();
    }

    public static int getIntConfig(Context contexto, String chave) {
        // seta o retorno
        int retorno = 0;

        try {
            retorno = (contexto.getSharedPreferences("config", Context.MODE_PRIVATE)).getInt(chave, Integer.MIN_VALUE);
        } catch (Exception ex) {
        }

        // retorna
        return retorno;
    }

    public static void gravaConfig(Context contexto, String chave, float valor) {
        SharedPreferences settings = contexto.getSharedPreferences("config", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(chave, valor);
        editor.commit();
    }
    public static int getFloatConfig(Context contexto, String chave) {
        // seta o retorno
        int retorno = 0;

        try {
            retorno = (contexto.getSharedPreferences("config", Context.MODE_PRIVATE)).getInt(chave, 0);
        }
        catch (Exception ex) {}

        // retorna
        return retorno;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static void showToast(Context context, int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                 v.vibrate(500);
    }
}