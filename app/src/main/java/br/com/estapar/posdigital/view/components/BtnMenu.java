package br.com.estapar.posdigital.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.R;

public class BtnMenu extends LinearLayout {
    /**
     * Constructor
     *
     * @param context
     * @param attrs
     */
    public BtnMenu(Context context, AttributeSet attrs) { super(context, attrs); init(context, attrs); }

    private void init(Context context, AttributeSet attrs) {
        // seta o Background
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        // pega o inflater
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // pega a View
        View view = inflater.inflate(R.layout.btn_menu, this);

        // pega os atributos
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BtnPaymentMethod);

        // get parameters
        final String paymentTxt = a.getString(R.styleable.BtnPaymentMethod_paymentTxt);
        final int paymentImg    = a.getResourceId(R.styleable.BtnPaymentMethod_paymentImg, 0);

        // recycle
        a.recycle();

        // get Text
        TextView txt = view.findViewById(R.id.txtPaymentCard);

        // set image
        ((ImageView) view.findViewById(R.id.imgPaymentCard)).setImageResource(paymentImg);

        if (!paymentTxt.isEmpty()) { txt.setText(paymentTxt);     }
        else                       { txt.setVisibility(View.GONE);}
    }
}

