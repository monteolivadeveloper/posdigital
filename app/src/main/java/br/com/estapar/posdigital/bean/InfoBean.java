package br.com.estapar.posdigital.bean;

import org.androidannotations.annotations.EBean;

import java.io.Serializable;

@EBean
public class InfoBean implements Serializable {
    private String nrSerie;

    public String getNrSerie() { return nrSerie; }

    public void setNrSerie(String nrSerie) { this.nrSerie = nrSerie; }
}