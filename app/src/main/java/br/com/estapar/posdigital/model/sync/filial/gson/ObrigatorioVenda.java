
package br.com.estapar.posdigital.model.sync.filial.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObrigatorioVenda {

    @SerializedName("setor")
    @Expose
    private Boolean setor;
    @SerializedName("vaga")
    @Expose
    private Boolean vaga;
    @SerializedName("placa")
    @Expose
    private Boolean placa;
    @SerializedName("tipoVeiculo")
    @Expose
    private Boolean tipoVeiculo;

    public Boolean getSetor() {
        return setor;
    }

    public void setSetor(Boolean setor) {
        this.setor = setor;
    }

    public Boolean getVaga() {
        return vaga;
    }

    public void setVaga(Boolean vaga) {
        this.vaga = vaga;
    }

    public Boolean getPlaca() {
        return placa;
    }

    public void setPlaca(Boolean placa) {
        this.placa = placa;
    }

    public Boolean getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(Boolean tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

}
