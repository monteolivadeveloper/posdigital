package br.com.estapar.posdigital.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Handler;
import android.view.KeyEvent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;

import br.com.estapar.posdigital.R;

@EActivity(R.layout.activity_splash_screen)
@Fullscreen
public class SplashScreen extends AppCompatActivity {
    private int seconds = 2000; // 2 seconds

    @AfterViews
    protected void afterViews() {
        new Handler().postDelayed(() -> {
            Intent login = new Intent(getBaseContext(), LoginActivity_.class);
            Intent main  = new Intent(getBaseContext(), MainActivity_.class);
            startActivity(login);
            finish();
        }, seconds);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }
}