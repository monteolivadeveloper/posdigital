
package br.com.estapar.posdigital.model.sync.filial.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilialResponse {

    @SerializedName("filial")
    @Expose
    private Filial filial;
    @SerializedName("codigoRetorno")
    @Expose
    private Integer codigoRetorno;
    @SerializedName("descricaoRetorno")
    @Expose
    private String descricaoRetorno;

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getDescricaoRetorno() {
        return descricaoRetorno;
    }

    public void setDescricaoRetorno(String descricaoRetorno) {
        this.descricaoRetorno = descricaoRetorno;
    }

}
