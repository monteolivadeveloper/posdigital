package br.com.estapar.posdigital.bean;

import java.io.Serializable;

public class WifiBean implements Serializable {
	private String macAddress;
	private int ipAddress;
	private String ssid;

	/**
	 * Constructor
	 */
	public WifiBean() {
		this.ssid       = "";
		this.ipAddress  = 0;
		this.macAddress = "";
	}

	/**
	 * Metodos Setter's
	 */
	public void setMacAddress(String m) { this.macAddress = m; }
	public void setIpAddress(int i)     { this.ipAddress  = i; }
	public void setSSID(String s)       { this.ssid       = s; }
	
	/**
	 * Metodos Getter's
	 */
	public String getMacAddress() { return this.macAddress; }
	public int getIpAddress()     { return this.ipAddress;  }
	public String getSSID()       { return this.ssid;       }
}