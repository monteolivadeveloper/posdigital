package br.com.estapar.posdigital.view.keyboard;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import com.getnet.posdigital.PosDigital;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnBtnKeyListener;
import br.com.estapar.posdigital.listener.OnBtnKeyLongListener;

public class Key extends LinearLayout implements View.OnClickListener, View.OnLongClickListener {
    private OnBtnKeyListener listener;
    private OnBtnKeyLongListener listenerLong;
    private String keyboardTxt;

    public Key(Context context, AttributeSet attrs) { super(context, attrs); init(context, attrs);}

    private void init(Context context, AttributeSet attrs) {
        // seta o Background
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        // pega o inflater
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // pega a View
        View view;

        // pega os atributos
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Key);

        // get parameters
        keyboardTxt = a.getString(R.styleable.Key_text);

        // recycle
        a.recycle();

        if (keyboardTxt.equals("BAC")) {
            view = inflater.inflate(R.layout.key_image, this);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }
        else if (keyboardTxt.equals("IR")) {
            view = inflater.inflate(R.layout.key_next, this);
            Button txt = view.findViewById(R.id.btnKeyNext);
            txt.setText(keyboardTxt);
            txt.setOnClickListener(this);
        }
        else {
            view = inflater.inflate(R.layout.key_button, this);
            Button txt = view.findViewById(R.id.btnKey);
            txt.setText(keyboardTxt);
            txt.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        beepDigit();
        if (listener != null) { listener.onKeyClick(keyboardTxt); }
    }

    @Override
    public boolean onLongClick(View view) {
        if (listenerLong != null) { listenerLong.onKeyLongClick(); }
        return false;
    }

    private void beepDigit() {
//        try {
//            PosDigital.getInstance().getBeeper().custom(0);
//        }
//        catch (RemoteException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Method Setter´s Listener
     * @param listener
     */
    public void setOnBtnKeyListener(OnBtnKeyListener listener)         { this.listener     = listener; }
    public void setOnBtnKeyLongListener(OnBtnKeyLongListener listener) { this.listenerLong = listener; }
}