package br.com.estapar.posdigital.model.sync.cashier;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import httpaction.HttpResponse;

import br.com.estapar.posdigital.model.sync.cashier.gson.CaixaResponse;
import br.com.estapar.posdigital.view.components.ActionConfirm;
import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.utils.http.RestCallback;
import br.com.estapar.posdigital.model.sync.cashier.listener.OnCashierListener;
import br.com.estapar.posdigital.model.sync.SyncType;

public class CashierModel {
    private Context context;
    private OnCashierListener listener;
    private PosDigitalApplication app;
    private CashierConfig config;
    private String loginUser;
    private String tokenUser;

    CashierModel(@NonNull Context context,
                 @NonNull PosDigitalApplication app,
                 @NonNull OnCashierListener listener) {
        this.context   = context;
        this.app       = app;
        this.listener  = listener;
        this.tokenUser = app.getUsuarioResponse().getUsuario().getAcessoUsuario().getToken();
        this.loginUser = app.getUsuarioResponse().getUsuario().getNome();

        this.config = new CashierConfig(context);
        this.config.setCallBack(callBack);
    }

    /**
     * Method to payment in Cash
     */
    public void payment() {
//        if (!app.isCashierOpen()) {
//            ActionConfirm confirm = new ActionConfirm.Builder().context(context).build();
//            confirm.setText(R.string.cashier_message_box);
//            confirm.setOkText(R.string.cashier_open_box);
//            confirm.setOnMsgCallback(() -> paymentCallback());
//            confirm.show();
//        }
//        else {
            if (listener != null) { listener.openSuccess(); }
//        }
    }

    private void paymentCallback() {
        if (listener != null) { listener.openCashierPage(); }
    }

    private void updatedCashier(HttpResponse response) { setResponse(response, false); }
    private void consultCashier(HttpResponse response) { setResponse(response, false); }
    private void closeCashier(HttpResponse response)   { setResponse(response, true);  }

    private void setResponse(HttpResponse response, boolean isClose) {
        CaixaResponse cashier = new Gson().fromJson(response.getMessage(), CaixaResponse.class);
        if (listener != null) {
            int codeHttp = response.getCodeHttp();
            if (codeHttp == 200 || codeHttp == 201) {
                boolean status = (isClose) ? false : cashier.getCaixa().getEstado().equals("aberto");
                app.setCashierOpen(status);
                app.setCaixaResponse(cashier);

                listener.openSuccess();
            }
            else { listener.openError(cashier.getDescricaoRetorno()); }
        }
    }

    /**************************************************************************************
     * Callback
     **************************************************************************************/
    private RestCallback callBack = new RestCallback() {
        @Override
        public void onResponse(HttpResponse response, int type) {
            switch (type) {
                case SyncType.ABERTURA_CAIXA:
                    updatedCashier(response);
                    break;
                case SyncType.CONSULTAR_CAIXA:
                    consultCashier(response);
                    break;
                case SyncType.SUPRIMENTO:
                    updatedCashier(response);
                    break;
                case SyncType.SANGRIA:
                    updatedCashier(response);
                    break;
                case SyncType.FECHAMENTO_CAIXA:
                    closeCashier(response);
                    break;
            }
        }

        @Override
        public void onError(HttpResponse httpResponse) {
            if (listener != null) { listener.openError(httpResponse.getMessage()); }
        }
    };

    /**************************************************************************************
     * Actions
     **************************************************************************************/
    public void abrirCaixa(@NonNull double valueOpen) {
        if (app.isCashierOpen()) {
            if (listener != null) {
                listener.openError(context.getString(R.string.cashier_opened));
            }
        }
        else {
            this.config.atualizarCaixa(SyncType.ABERTURA_CAIXA,
                    loginUser,
                    app.IMEI,
                    tokenUser,
                    valueOpen);
        }
    }
    public void consultarCaixa() {
        this.config.consultCashier(loginUser,
                app.IMEI,
                tokenUser);
    }
    public void suprimento(@NonNull double valueOpen) {
        this.config.atualizarCaixa(SyncType.SUPRIMENTO,
                loginUser,
                app.IMEI,
                tokenUser,
                valueOpen);
    }
    public void sangria(@NonNull double valueOpen) {
        this.config.atualizarCaixa(SyncType.SANGRIA,
                loginUser,
                app.IMEI,
                tokenUser,
                valueOpen);
    }
    public void fecharCaixa() {
        this.config.atualizarCaixa(SyncType.FECHAMENTO_CAIXA,
                loginUser,
                app.IMEI,
                tokenUser,
                0);
    }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;
        private PosDigitalApplication app;
        private OnCashierListener listener;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder application(PosDigitalApplication app) {
            this.app = app;
            return this;
        }

        public Builder listener(OnCashierListener listener) {
            this.listener = listener;
            return this;
        }

        public CashierModel build() {
            if (app == null) {
                throw new IllegalStateException("Application required.");
            }
            else if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            else if (listener == null) {
                throw new IllegalStateException("Listener required.");
            }
            return new CashierModel(context, app, listener);
        }
    }
}