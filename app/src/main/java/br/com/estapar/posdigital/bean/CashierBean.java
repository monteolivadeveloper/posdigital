package br.com.estapar.posdigital.bean;

import org.androidannotations.annotations.EBean;

import java.io.Serializable;

@EBean
public class CashierBean implements Serializable {
    private boolean isOpen;
    private double balance;

    /**
     * Constructor
     */
    public CashierBean() {
        this.isOpen  = false;
        this.balance = 0;
    }

    /**
     * methods for Balance
     * @return
     */
    public double getBalance()             { return balance;  }
    public void setBalance(double balance) { this.balance = balance; }

    /**
     * Methds to Open CaixaResponse
     * @param isOpen
     */
    public void setOpen(boolean isOpen) { this.isOpen = isOpen; }
    public boolean isOpen()             { return isOpen; }

}