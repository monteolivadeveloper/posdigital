package br.com.estapar.posdigital.model.mifare;

import android.content.Context;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnCallbackListener;

public class MifareBoxNew {
    private AlertDialog alertDialog;
    private AlertDialog.Builder dialog;
    private Context context;
    private TextView txtNfcMsg;
    private TextView txtNfcSerial;
    private MifareModel model;
    private AppCompatActivity compat;
    private OnCallbackListener listener;

    private int type = MifareType.TYPE_CANCEL;

    public static MifareBoxNew getInstance(@NonNull  Context context) {
        return new MifareBoxNew(context);
    }

    private MifareBoxNew(@NonNull Context context) {
        this.context = context;
    }

    public void open(@NonNull int message) {
        // set type
        this.type = MifareType.TYPE_CANCEL;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.nfc_mifire_new, null);

        compat = (AppCompatActivity) context;

        txtNfcMsg    = view.findViewById(R.id.txtNfcMsg);
        txtNfcSerial = view.findViewById(R.id.txtNfcSerial);
        txtNfcMsg.setText(message);
        txtNfcSerial.setText("");

        dialog = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        dialog.setView(view);
        dialog.setCancelable(false);
        dialog.setNegativeButton(R.string.btn_close, (dialog, whichButton) -> {
            if (listener != null) { listener.onCallbackSuccess(type); }
        });
        dialog.setPositiveButton(R.string.btn_ok, (dialog, whichButton) -> {
            if (listener != null) { listener.onCallbackSuccess(type); }
        });

        alertDialog = dialog.create();
        alertDialog.show();
    }

    public void initMifare() {
        model = MifareModel.getInstance(this.context);
        model.setCallback(this);
    }

    public void onFinish(@NonNull String message) {
        compat.runOnUiThread (new Thread(() -> {
            type = MifareType.TYPE_FINISH;
            txtNfcMsg.setText(message);
        }));
    }
    public void onSerial(@NonNull String message) {
        compat.runOnUiThread (new Thread(() -> txtNfcSerial.setText(message)));
    }
    public void onCancel(@NonNull String message) {
        compat.runOnUiThread (new Thread(() -> {
            type = MifareType.TYPE_CANCEL;
            txtNfcMsg.setText(message);
        }));
    }

    public void setDataUser(double dataUser) { model.setDataUser(dataUser); }

    /**
     * Method Setter for Listener CallBack
     *
     * @param listener
     */
    public void setOnCallbackListener(OnCallbackListener listener) { this.listener = listener; }

    /**************************************************************************
     * Methods for Mifare
     **************************************************************************/
    public void decrementValue() throws RemoteException { model.decrementValue();  }
    public void incrementValue() throws RemoteException { model.incrementValue();  }
    public void blankCard()      throws RemoteException { model.blankMifareCard(); }
    public void balance()        throws RemoteException { model.balance();         }

    public void clearBlock(int sector, int block) throws RemoteException {
        model.clearBlock(sector, block);
    }
}