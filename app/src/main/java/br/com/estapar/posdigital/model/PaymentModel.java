package br.com.estapar.posdigital.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.model.enums.ARG;
import br.com.estapar.posdigital.utils.Constantes;
import br.com.estapar.posdigital.utils.Utils;

public class PaymentModel {
    private Context context;
    private AppCompatActivity compat;

    /**
     * Constructor
     *
     * @param context
     */
    public PaymentModel(Context context) {
        this.context = context;
        this.compat  = (AppCompatActivity) context;
    }

    public void payment(@NonNull String currency_value, Constantes.Method type) {
        currency_value = currency_value.replace(",", "");
        currency_value = currency_value.replace(".", "");
        currency_value = Utils.strZero(currency_value, 12);

        if      (type == Constantes.Method.DEBIT ) { debit(currency_value);  }
        else if (type == Constantes.Method.CREDIT) { credit(currency_value); }
    }

    /**
     * Method for payment DEBIT
     *
     * @param currency_value
     */
    private void debit(@NonNull String currency_value) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG.AMOUNT.get(), currency_value);
        bundle.putString("paymentType", "debit");

        openPaymentActivity(bundle);
    }

    /**
     * Method for payment CREBIT
     *
     * @param currency_value
     */
    private void credit(@NonNull String currency_value) {
        currency_value = currency_value.replace(",", "");
        currency_value = currency_value.replace(".", "");
        currency_value = Utils.strZero(currency_value, 12);

        Bundle bundle = new Bundle();
        bundle.putString(ARG.AMOUNT.get(), currency_value);
        bundle.putString("paymentType", "credit");

        openPaymentActivity(bundle);
    }

    private void openPaymentActivity(Bundle bundle) {
        bundle.putString("currencyPosition", "CURRENCY_BEFORE_AMOUNT");
        bundle.putString("currencyCode", "986");

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("getnet://pagamento/v1/payment"));
        intent.putExtras(bundle);

        compat.startActivityForResult(intent, Constantes.REQUEST_CODE_CARD);
    }
}