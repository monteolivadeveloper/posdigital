package br.com.estapar.posdigital.model.sync.user;

import android.content.Context;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import httpaction.HttpCallBack;
import httpaction.HttpResponse;

import br.com.estapar.posdigital.BuildConfig;
import br.com.estapar.posdigital.model.enums.Endpoints;
import br.com.estapar.posdigital.utils.http.RestCallback;
import br.com.estapar.posdigital.utils.http.RestService;
import br.com.estapar.posdigital.model.sync.SyncType;

public class UserConfig {
    private String BASE_URL = BuildConfig.BASE_URL;
    private RestService service;
    private RestCallback callBack;

    public UserConfig(Context context) {
        service = new RestService.Builder()
                .baseUrl(BASE_URL)
                .context(context)
                .build();
    }

    public void authenticate(@NonNull String userLogin,
                             @NonNull String imei,
                             @NonNull String passLogin) {

        String wsEndpoint = Endpoints.USUARIO_AUTENTICAR.endpoint();
        String uri        = MessageFormat.format(wsEndpoint, userLogin);

        Map<String, String> requestParam = new HashMap<>();
                            requestParam.put("dispositivo", imei);

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("senha", passLogin);
        }
        catch (JSONException e) { e.printStackTrace(); }

        service.callWsPost(uri,
                requestParam,
                jsonBody,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse httpResponse) {
                        if (callBack != null) {
                            callBack.onResponse(httpResponse, SyncType.USER_AUTENTICAR);
                        }
                    }

                    @Override
                    public void onError(HttpResponse httpResponse) {
                        if (callBack != null) { callBack.onError(httpResponse); }
                    }
                });
    }

    public void consult(@NonNull String userLogin,
                        @NonNull String imei,
                        @NonNull String token) {
        String wsEndpoint = Endpoints.USUARIO_CONSULTAR.endpoint();
        String uri        = MessageFormat.format(wsEndpoint, userLogin);

        Map<String, String> requestParam = new HashMap<>();
        requestParam.put("dispositivo", imei);
        requestParam.put("token", token);

        service.callWsGet(uri,
                requestParam,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse httpResponse) {
                        if (callBack != null) {
                            callBack.onResponse(httpResponse, SyncType.USER_CONSULT);
                        }
                    }

                    @Override
                    public void onError(HttpResponse httpResponse) {
                        if (callBack != null) { callBack.onError(httpResponse); }
                    }
                });
    }

    public void setCallBack(RestCallback callBack) { this.callBack = callBack; }
}