package br.com.estapar.posdigital.model.sync;

public interface SyncType {
    // códigos de operação do caixa
    int ABERTURA_CAIXA   = 1;
    int FECHAMENTO_CAIXA = 3;
    int VENDA            = 4;
    int SANGRIA          = 5;
    int SUPRIMENTO       = 7;
    int CONSULTAR_CAIXA  = 8;

    int FILIAL_TARIFA   = 2001;
    int USER_AUTENTICAR = 2002;
    int USER_CONSULT    = 2003;
}