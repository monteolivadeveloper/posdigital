package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnValuesListener;
import br.com.estapar.posdigital.utils.Utils;
import br.com.estapar.posdigital.view.components.BtnValues;
import br.com.estapar.posdigital.view.keyboard.KeyBoard;

@EActivity(R.layout.activity_sale)
@Fullscreen
public class SaleActivity extends AppCompatActivity implements OnValuesListener {
    @ViewById(R.id.btnOpt1)
    protected BtnValues btnOpt1;
    @ViewById(R.id.btnOpt2)
    protected BtnValues btnOpt2;
    @ViewById(R.id.btnOpt3)
    protected BtnValues btnOpt3;
    @ViewById(R.id.btnOpt4)
    protected BtnValues btnOpt4;
    @ViewById(R.id.btnOpt5)
    protected BtnValues btnOpt5;
    @ViewById(R.id.btnOpt6)
    protected BtnValues btnOpt6;
    @ViewById(R.id.editPlaca)
    protected EditText editText;

    @ViewById(R.id.saleLayout)
    protected LinearLayout saleLayout;

    private String valueSelected;
    private String tarifaSelected;
    private int minutesSelected;

    // Keyboard
    @ViewById(R.id.keyboard)
    protected KeyBoard keyBoard;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        keyBoard.setTextView(editText);
        keyBoard.setActivity(this);

        btnOpt1.setOnValuesListener(this);
        btnOpt2.setOnValuesListener(this);
        btnOpt3.setOnValuesListener(this);
        btnOpt4.setOnValuesListener(this);
        btnOpt5.setOnValuesListener(this);
        btnOpt6.setOnValuesListener(this);

        // initialize
        valueSelected   = "";
        tarifaSelected  = "";
        minutesSelected = 0;

        saleLayout.setOnClickListener(view -> Utils.hideNavigationBar(SaleActivity.this));
    }

    private void back() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    @Override
    public void onClick(@NonNull String tarifa, @NonNull int minutes, @NonNull String value) {
        clearSelection();

        if      (btnOpt1.getRetValue().equals(value)) { btnOpt1.setSelected(true); }
        else if (btnOpt2.getRetValue().equals(value)) { btnOpt2.setSelected(true); }
        else if (btnOpt3.getRetValue().equals(value)) { btnOpt3.setSelected(true); }
        else if (btnOpt4.getRetValue().equals(value)) { btnOpt4.setSelected(true); }
        else if (btnOpt5.getRetValue().equals(value)) { btnOpt5.setSelected(true); }
        else if (btnOpt6.getRetValue().equals(value)) { btnOpt6.setSelected(true); }

        this.valueSelected   = value;
        this.tarifaSelected  = tarifa;
        this.minutesSelected = minutes;
    }

    public void clearSelection() {
        btnOpt1.setSelected(false);
        btnOpt2.setSelected(false);
        btnOpt3.setSelected(false);
        btnOpt4.setSelected(false);
        btnOpt5.setSelected(false);
        btnOpt6.setSelected(false);

        this.valueSelected   = "";
        this.tarifaSelected  = "";
        this.minutesSelected = 0;
    }

    /**
     * Method to go next activity
     */
    public void next() {
        String nrPlaca = editText.getEditableText().toString();

        if (nrPlaca.isEmpty()) {
            Utils.msgBox(this, R.string.txt_placa_error);
            return;
        }
        else if (this.valueSelected.isEmpty()) {
            Utils.msgBox(this, R.string.txt_value_error);
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putString("PLACA", nrPlaca);
        bundle.putString("TIME_VALUE", valueSelected);
        bundle.putString("TIME_TARIFA", tarifaSelected);
        bundle.putInt("TIME_MINUTES", minutesSelected);

        Intent intent = new Intent(this, MethodActivity_.class);
        intent.putExtras(bundle);

        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { back(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }
}