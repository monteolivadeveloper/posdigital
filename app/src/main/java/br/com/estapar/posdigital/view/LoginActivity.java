package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.model.sync.listener.OnSyncListener;
import br.com.estapar.posdigital.model.sync.user.UserModel;
import br.com.estapar.posdigital.utils.Utils;

@EActivity(R.layout.activity_login)
@Fullscreen
public class LoginActivity extends DefaultActivity {
    @ViewById(R.id.userLogin)
    protected TextInputEditText userLogin;
    @ViewById(R.id.passLogin)
    protected TextInputEditText passLogin;
    @ViewById(R.id.userLoginInput)
    protected TextInputLayout userLoginInput;
    @ViewById(R.id.passLoginInput)
    protected TextInputLayout passLoginInput;

    private UserModel model;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        model = new UserModel.Builder()
                .context(this)
                .application(app)
                .listener(userListener)
                .build();

        String uLogin = Utils.getStringConfig(this, "login_login");
        userLogin.setText(uLogin);

        passLogin.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) { enter(); }
            return false;
        });
    }

    @Click(R.id.btnLoginEnter)
    protected void enter() {
        String login    = userLogin.getEditableText().toString();
        String password = passLogin.getEditableText().toString();
        String errorMsg = "";
        boolean isLogar = true;

        if (login.isEmpty()) {
            errorMsg = getString(R.string.login_field1_error);
            isLogar  = false;
        }
        else if (password.isEmpty()) {
            errorMsg = getString(R.string.login_field2_error);
            isLogar  = false;
        }

        if (isLogar) {
            progress();
            model.autenticarUsuario(login, password);
        }
        else {
            msgError(errorMsg);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { finish(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { finish(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    @Override
    protected void resume() {}

    /*****************************************************************************
     * CallBack (listener)
     *****************************************************************************/
    private OnSyncListener userListener = new OnSyncListener() {
        @Override
        public void syncSuccess() {
            final Intent intent = new Intent(getBaseContext(), MainActivity_.class);
            msgSuccess(R.string.login_success, () -> {
                close();
                startActivity(intent);
                finish();
            });
        }

        @Override
        public void syncError(String message) { msgError(message); }
    };
}