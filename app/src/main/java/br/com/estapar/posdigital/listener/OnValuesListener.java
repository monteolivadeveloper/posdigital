package br.com.estapar.posdigital.listener;

import androidx.annotation.NonNull;

public interface OnValuesListener {
    void onClick(@NonNull String tarifa, @NonNull int minutes, @NonNull String value);
}