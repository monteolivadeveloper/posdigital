package br.com.estapar.posdigital.model.mifare;

public interface MifareType {
    int S50_CARD     = 0;
    int S70_CARD     = 1;
    int PRO_CARD     = 2;
    int S50_PRO_CARD = 3;
    int S70_PRO_CARD = 4;
    int CPU_CARD     = 5;

    int TYPE_FINISH = 0;
    int TYPE_CANCEL = 1;

    byte[] KEY         = new byte[] { (byte) 0xDA, (byte) 0xFF, (byte) 0xAE, (byte) 0x9F, (byte) 0x7E, (byte) 0x55 };
    byte[] KEY_DEFAULT = new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };
    byte[] KEY_MD      = new byte[] { (byte) 0xA0, (byte) 0xA1, (byte) 0xA2, (byte) 0xA3, (byte) 0xA4, (byte) 0xA5 };
}