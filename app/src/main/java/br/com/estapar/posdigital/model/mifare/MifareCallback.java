package br.com.estapar.posdigital.model.mifare;

import android.os.RemoteException;

public interface MifareCallback {
    void onSuccess(int type) throws RemoteException;
    void onError(String message) throws RemoteException;
}