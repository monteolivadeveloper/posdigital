package br.com.estapar.posdigital.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.widget.TextView;

import com.getnet.posdigital.PosDigital;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.bean.PrintBean;
import br.com.estapar.posdigital.listener.OnCallbackListener;
import br.com.estapar.posdigital.model.PaymentModel;
import br.com.estapar.posdigital.model.mifare.MifareBoxNew;
import br.com.estapar.posdigital.model.mifare.MifareType;
import br.com.estapar.posdigital.model.mifare.MifareUtils;
import br.com.estapar.posdigital.model.printer.PrintTicketSaoPaulo;
import br.com.estapar.posdigital.utils.Constantes;
import br.com.estapar.posdigital.view.components.Progress;

@EActivity(R.layout.activity_method)
@Fullscreen
public class MethodActivity extends AppCompatActivity implements OnCallbackListener {
    @ViewById(R.id.ticketPlaca)
    protected TextView ticketPlaca;
    @ViewById(R.id.ticketTime)
    protected TextView ticketTime;
    @ViewById(R.id.ticketValue)
    protected TextView ticketValue;

    @App
    protected PosDigitalApplication app;

    protected PaymentModel paymentModel;

    private Progress progress;
    private String PLACA;
    private String TIME_VALUE;
    private String TIME_TARIFA;
    private int TIME_MINUTES;
    private MifareBoxNew mifareBox;

    @AfterViews
    protected void afterViews() {
        // get Bundle arguments
        Bundle bundle = getIntent().getExtras();
        PLACA         = bundle.getString("PLACA");
        TIME_VALUE    = bundle.getString("TIME_VALUE");
        TIME_TARIFA   = bundle.getString("TIME_TARIFA");
        TIME_MINUTES  = bundle.getInt("TIME_MINUTES", 30);

        // Progress
        progress = new Progress.Builder().context(this).build();

        // Payment
        paymentModel = new PaymentModel(this);

        String txtTicketPlaca = getString(R.string.txt_ticket_placa);
        String txtTicketTime  = getString(R.string.txt_ticket_time);
        String txtTicketValue = getString(R.string.txt_ticket_value);

        ticketPlaca.setText(txtTicketPlaca + " " + PLACA);
        ticketValue.setText(txtTicketValue + " " + TIME_VALUE);
        ticketTime.setText(txtTicketTime + " " + TIME_TARIFA);

        mifareBox = MifareBoxNew.getInstance(this);
        mifareBox.initMifare();
        mifareBox.setOnCallbackListener(this);
    }

    @Click(R.id.btnDebit)
    protected void paymentDebit() {
        if (PosDigital.getInstance().isInitiated()) {
            progress.show();
            paymentModel.payment(TIME_VALUE, Constantes.Method.DEBIT);
        }
    }

    @Click(R.id.btnCredit)
    protected void paymentCredit() {
        if (PosDigital.getInstance().isInitiated()) {
            progress.show();
            paymentModel.payment(TIME_VALUE, Constantes.Method.CREDIT);
        }
    }

    @Click(R.id.btnNFC)
    protected void paymentNFC() {
        try {
            mifareBox.setDataUser(MifareUtils.getDoubleValue(TIME_VALUE));
            mifareBox.decrementValue();
        }
        catch (RemoteException re) {}
    }

    @Click(R.id.btnCash)
    protected void paymentCash() { print("Dinheiro"); }

    private void print(String formaPagto) {
        if (PosDigital.getInstance().isInitiated()) {
            progress.show();

            // Print Bean
            PrintBean bean = new PrintBean();
            bean.setNrPlaca(PLACA);
            bean.setTarifa(TIME_TARIFA);
            bean.setMinutes(TIME_MINUTES);
            bean.setValue(TIME_VALUE);
            bean.setFormaPagto(formaPagto);

            PrintTicketSaoPaulo printer = new PrintTicketSaoPaulo(app);
            printer.setOnCallbackListener(new OnCallbackListener() {
                @Override
                public void onCallbackSuccess(@NonNull int type) {
                    progress.hide();
                }

                @Override
                public void onCallbackError(@NonNull String message) {
                }
            });
            printer.print(bean);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            progress.hide();
            if (requestCode == Constantes.REQUEST_CODE_CARD) {

            }
        }
    }

    private void back() {
        startActivity(new Intent(this, SaleActivity_.class));
        finish();
    }

    private void menu() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    /**
     * Evento KeyDown
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { menu(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /**************************************************************************
     * Methods for Mifare CallBack
     **************************************************************************/
    @Override
    public void onCallbackSuccess(@NonNull int type) {
        if (type == MifareType.TYPE_FINISH) {
            print("Cartao NFC");
            back();
        }
    }

    @Override
    public void onCallbackError(@NonNull String message) {}
}