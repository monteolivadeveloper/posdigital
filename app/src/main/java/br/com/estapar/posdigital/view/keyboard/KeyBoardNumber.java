package br.com.estapar.posdigital.view.keyboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnBtnKeyListener;
import br.com.estapar.posdigital.listener.OnBtnKeyLongListener;

public class KeyBoardNumber extends LinearLayout implements OnBtnKeyListener, OnBtnKeyLongListener {
    private Key btnKey0, btnKey1, btnKey2, btnKey3, btnKey4, btnKey5, btnKey6;
    private Key btnKey7, btnKey8, btnKey9;

    // Specials keys
    private Key btnKeyBAC;

    private EditText textView;

    public KeyBoardNumber(Context context)                     { super(context);        init(context); }
    public KeyBoardNumber(Context context, AttributeSet attrs) { super(context, attrs); init(context); }

    private void init(Context context) {
        // seta o Background
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        // pega o inflater
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // pega a View
        View view = inflater.inflate(R.layout.keyboard_number, this);

        // Numbers Keys
        btnKey0 = view.findViewById(R.id.btnKey0N);
        btnKey1 = view.findViewById(R.id.btnKey1N);
        btnKey2 = view.findViewById(R.id.btnKey2N);
        btnKey3 = view.findViewById(R.id.btnKey3N);
        btnKey4 = view.findViewById(R.id.btnKey4N);
        btnKey5 = view.findViewById(R.id.btnKey5N);
        btnKey6 = view.findViewById(R.id.btnKey6N);
        btnKey7 = view.findViewById(R.id.btnKey7N);
        btnKey8 = view.findViewById(R.id.btnKey8N);
        btnKey9 = view.findViewById(R.id.btnKey9N);

        // Specials Keys
        btnKeyBAC = view.findViewById(R.id.btnKeyBACN);

        // set Listener
        btnKey0.setOnBtnKeyListener(this);
        btnKey1.setOnBtnKeyListener(this);
        btnKey2.setOnBtnKeyListener(this);
        btnKey3.setOnBtnKeyListener(this);
        btnKey4.setOnBtnKeyListener(this);
        btnKey5.setOnBtnKeyListener(this);
        btnKey6.setOnBtnKeyListener(this);
        btnKey7.setOnBtnKeyListener(this);
        btnKey8.setOnBtnKeyListener(this);
        btnKey9.setOnBtnKeyListener(this);




        btnKeyBAC.setOnBtnKeyListener(this);




    }

    public void show() { this.setVisibility(View.VISIBLE); }
    public void hide() { this.setVisibility(View.GONE);    }

    @Override
    public void onKeyClick(String value) {
        if (textView != null) {
            String textOld = textView.getEditableText().toString();

            // verify text value
            if (!value.isEmpty()) {
                if (value.equals("BAC")) {
                    int len = textOld.length();
                    if (len > 0) {
                        textOld = textOld.substring(0, (len - 1));
                    }
                }
                else {
                    textOld += value;
                }

                textView.setText(textOld);
            }
        }
    }

    @Override
    public void onKeyLongClick() {
        textView.setText("");
        //activity.clearSelection();
    }

    public void setEdit(EditText textView) { this.textView = textView; }
}