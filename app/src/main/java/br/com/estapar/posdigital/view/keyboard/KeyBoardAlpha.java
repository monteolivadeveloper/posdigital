package br.com.estapar.posdigital.view.keyboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnBtnKeyListener;
import br.com.estapar.posdigital.listener.OnBtnKeyLongListener;
import br.com.estapar.posdigital.view.SaleActivity;

public class KeyBoardAlpha extends LinearLayout implements OnBtnKeyListener, OnBtnKeyLongListener {
    private Key btnKeyA, btnKeyB, btnKeyC, btnKeyD, btnKeyE, btnKeyF, btnKeyG;
    private Key btnKeyH, btnKeyI, btnKeyJ, btnKeyK, btnKeyL, btnKeyM, btnKeyN;
    private Key btnKeyO, btnKeyP, btnKeyQ, btnKeyR, btnKeyS, btnKeyT, btnKeyU;
    private Key btnKeyV, btnKeyX, btnKeyY, btnKeyZ, btnKeyW;

    // Spacials keys
    private Key btnKeyBAC, btnKeyNEX;

    private TextView textView;
    private SaleActivity activity;

    public KeyBoardAlpha(Context context)                     { super(context);        init(context); }
    public KeyBoardAlpha(Context context, AttributeSet attrs) { super(context, attrs); init(context); }

    private void init(Context context) {
        // seta o Background
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        // pega o inflater
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // pega a View
        View view = inflater.inflate(R.layout.keyboard_alpha, this);

        // get keys
        btnKeyA = view.findViewById(R.id.btnKeyA);
        btnKeyB = view.findViewById(R.id.btnKeyB);
        btnKeyC = view.findViewById(R.id.btnKeyC);
        btnKeyD = view.findViewById(R.id.btnKeyD);
        btnKeyE = view.findViewById(R.id.btnKeyE);
        btnKeyF = view.findViewById(R.id.btnKeyF);
        btnKeyG = view.findViewById(R.id.btnKeyG);
        btnKeyH = view.findViewById(R.id.btnKeyH);
        btnKeyI = view.findViewById(R.id.btnKeyI);
        btnKeyJ = view.findViewById(R.id.btnKeyJ);
        btnKeyK = view.findViewById(R.id.btnKeyK);
        btnKeyL = view.findViewById(R.id.btnKeyL);
        btnKeyM = view.findViewById(R.id.btnKeyM);
        btnKeyN = view.findViewById(R.id.btnKeyN);
        btnKeyO = view.findViewById(R.id.btnKeyO);
        btnKeyP = view.findViewById(R.id.btnKeyP);
        btnKeyQ = view.findViewById(R.id.btnKeyQ);
        btnKeyR = view.findViewById(R.id.btnKeyR);
        btnKeyS = view.findViewById(R.id.btnKeyS);
        btnKeyT = view.findViewById(R.id.btnKeyT);
        btnKeyU = view.findViewById(R.id.btnKeyU);
        btnKeyV = view.findViewById(R.id.btnKeyV);
        btnKeyX = view.findViewById(R.id.btnKeyX);
        btnKeyY = view.findViewById(R.id.btnKeyY);
        btnKeyZ = view.findViewById(R.id.btnKeyZ);
        btnKeyW = view.findViewById(R.id.btnKeyW);

        // Specials Keys
        btnKeyBAC = view.findViewById(R.id.btnKeyBAC);
        btnKeyNEX = view.findViewById(R.id.btnKeyNEXT);

        // set Listener
        btnKeyA.setOnBtnKeyListener(this);
        btnKeyB.setOnBtnKeyListener(this);
        btnKeyC.setOnBtnKeyListener(this);
        btnKeyD.setOnBtnKeyListener(this);
        btnKeyE.setOnBtnKeyListener(this);
        btnKeyF.setOnBtnKeyListener(this);
        btnKeyG.setOnBtnKeyListener(this);
        btnKeyH.setOnBtnKeyListener(this);
        btnKeyI.setOnBtnKeyListener(this);
        btnKeyJ.setOnBtnKeyListener(this);
        btnKeyK.setOnBtnKeyListener(this);
        btnKeyL.setOnBtnKeyListener(this);
        btnKeyM.setOnBtnKeyListener(this);
        btnKeyN.setOnBtnKeyListener(this);
        btnKeyO.setOnBtnKeyListener(this);
        btnKeyP.setOnBtnKeyListener(this);
        btnKeyQ.setOnBtnKeyListener(this);
        btnKeyR.setOnBtnKeyListener(this);
        btnKeyS.setOnBtnKeyListener(this);
        btnKeyT.setOnBtnKeyListener(this);
        btnKeyU.setOnBtnKeyListener(this);
        btnKeyV.setOnBtnKeyListener(this);
        btnKeyX.setOnBtnKeyListener(this);
        btnKeyY.setOnBtnKeyListener(this);
        btnKeyZ.setOnBtnKeyListener(this);
        btnKeyW.setOnBtnKeyListener(this);
        btnKeyNEX.setOnBtnKeyListener(this);
        btnKeyBAC.setOnBtnKeyListener(this);
        btnKeyBAC.setOnBtnKeyLongListener(this);
    }

    @Override
    public void onKeyClick(String value) {
        if (textView != null) {
            String textOld = textView.getText().toString();

            // verify text value
            if (!value.isEmpty()) {
                if (value.equals("BAC")) {
                    int len = textOld.length();
                    if (len > 0) {
                        textOld = textOld.substring(0, (len - 1));
                    }
                }
                else if (value.equals("IR")) {
                    if (activity != null) {
                        activity.next();
                    }
                }
                else {
                    textOld += value;
                }

                textView.setText(textOld);
            }
        }
    }

    @Override
    public void onKeyLongClick() {
        textView.setText("");
        activity.clearSelection();
    }

    public void setTextView(TextView textView)     { this.textView = textView; }
    public void setActivity(SaleActivity activity) { this.activity = activity; }
}