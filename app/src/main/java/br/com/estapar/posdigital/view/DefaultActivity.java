package br.com.estapar.posdigital.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.view.components.ActionMsg;

public abstract class DefaultActivity extends AppCompatActivity {
    private ActionMsg actionMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionMsg = new ActionMsg.Builder().context(this).build();
    }

    protected void close() { actionMsg = null; }

    @Override
    protected void onResume() {
        super.onResume();
        //resume();
    }

    /**
     * Progress
     */
    protected void progress() {
        actionMsg.hideBtnCancel();
        actionMsg.hideBtnOk();
        actionMsg.hideMessage();
        actionMsg.showProgress();
        actionMsg.setProgressTextDefault();
        actionMsg.show();
    }
    protected void progress(@NonNull String message) {
        actionMsg.hideBtnCancel();
        actionMsg.hideBtnOk();
        actionMsg.hideMessage();
        actionMsg.showProgress();
        actionMsg.setProgressText(message);
        actionMsg.show();
    }
    protected void progress(@NonNull int message) {
        if (actionMsg != null) {
            actionMsg.hideBtnCancel();
            actionMsg.hideBtnOk();
            actionMsg.hideMessage();
            actionMsg.showProgress();
            actionMsg.setProgressText(message);
            actionMsg.show();
        }
    }

    /**
     * Action Confirm
     */
    protected void msgConfirm(@NonNull String message,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.showMessage();
        actionMsg.showBtnCancel();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }
    protected void msgConfirm(@NonNull int message,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.showMessage();
        actionMsg.showBtnCancel();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }
    protected void msgConfirm(@NonNull String message,
                              @NonNull String btnOkText,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.showMessage();
        actionMsg.showBtnCancel();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setOkText(btnOkText);
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }
    protected void msgConfirm(@NonNull String message,
                              @NonNull int btnOkText,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.showMessage();
        actionMsg.showBtnCancel();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setOkText(btnOkText);
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }
    protected void msgConfirm(@NonNull int message,
                              @NonNull int btnOkText,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.hideMessage();
        actionMsg.showBtnCancel();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setOkText(btnOkText);
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }

    /**
     * Action Success
     */
    protected void msgSuccess(@NonNull String message,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.hideBtnCancel();
        actionMsg.showMessage();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }
    protected void msgSuccess(@NonNull int message,
                              @NonNull onCallback callback) {
        actionMsg.hideProgress();
        actionMsg.hideMessage();
        actionMsg.hideBtnCancel();
        actionMsg.showMessage();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.setOnMsgCallback(() -> callback.callback());
        actionMsg.show();
    }

    /**
     * Action Error
     */
    protected void msgError(@NonNull String message) {
        actionMsg.hideProgress();
        actionMsg.hideBtnCancel();
        actionMsg.showMessage();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.show();
    }
    protected void msgError(@NonNull int message) {
        actionMsg.hideProgress();
        actionMsg.hideBtnCancel();
        actionMsg.showMessage();
        actionMsg.showBtnOk();
        actionMsg.setProgressTextDefault();
        actionMsg.setText(message);
        actionMsg.show();
    }

    protected void hideMsg() { actionMsg.hide(); }

    protected interface onCallback { void callback(); }

    protected abstract void resume();
}