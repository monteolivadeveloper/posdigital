package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.view.KeyEvent;

import com.getnet.posdigital.PosDigital;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.model.printer.PrintAberturaCaixa;
import br.com.estapar.posdigital.model.sync.cashier.listener.OnCashierListener;
import br.com.estapar.posdigital.model.sync.cashier.CashierModel;
import br.com.estapar.posdigital.model.mifare.MifareUtils;
import br.com.estapar.posdigital.utils.Utils;
import br.com.estapar.posdigital.view.components.CurrencyEdit;

@EActivity(R.layout.activity_cashier_open)
@Fullscreen
public class CashierOpenActivity extends DefaultActivity {
    private CashierModel model;

    @ViewById(R.id.editOpenValue)
    protected CurrencyEdit editValue;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        model = new CashierModel.Builder()
                .context(this)
                .application(app)
                .listener(cashierListener)
                .build();
    }

    @Click(R.id.btnOpenSave)
    protected void save() {
        double saldo   = MifareUtils.getDoubleValue(editValue.getValue());
        String saldoEx = Utils.setFormatDecimal(saldo);
        String message = getString(R.string.cashier_confirm).replace("{0}", saldoEx);

        msgConfirm(message, () -> actSave());
    }

    private void actSave() {
        progress();
        double saveValue = MifareUtils.getDoubleValue(editValue.getValue());
        model.abrirCaixa(saveValue);
    }

    private String showBalance() {
        double saldo        = app.getCaixaResponse().getCaixa().getSaldo();
        String valueBalance = Utils.setFormatDecimal(saldo);
        String txtBalance   = getString(R.string.txt_balance) + " " + valueBalance;

        return txtBalance;
    }

    private void open() {
        double saveValue  = MifareUtils.getDoubleValue(editValue.getValue());
        String txtBalance = showBalance();
        String message    = getString(R.string.cashier_open_success) + "\n\n" + txtBalance;

        if (PosDigital.getInstance().isInitiated()) {
            new PrintAberturaCaixa.Builder().application(app).value(saveValue).build();
        }

        msgSuccess(message, () -> back());
    }

    private void back() {
        startActivity(new Intent(this, CashierActivity_.class));
        finish();
    }

    private void menu() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    /**
     * Evento KeyDown
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { menu(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    @Override
    protected void resume() {}

    /*****************************************************************************
     * CallBack (listener)
     *****************************************************************************/
    private OnCashierListener cashierListener = new OnCashierListener() {
        @Override
        public void openSuccess() { open(); }

        @Override
        public void openError(String message) { msgError(message); }

        @Override
        public void openCashierPage() {}
    };
}