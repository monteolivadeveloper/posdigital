package br.com.estapar.posdigital.utils.http;

import httpaction.HttpResponse;

public interface RestCallback {
    void onResponse(HttpResponse response, int type);
    void onError(HttpResponse response);
}