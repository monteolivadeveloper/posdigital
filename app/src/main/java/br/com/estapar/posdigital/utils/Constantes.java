package br.com.estapar.posdigital.utils;

public interface Constantes {
    // arguments
    int REQUEST_CODE_CARD = 1001;

    // Menu Type on Hone Window
    int MENU_HOME_TICKET        = 11;
    int MENU_HOME_CASHIER       = 12;
    int MENU_HOME_MIFARE        = 13;
    int MENU_HOME_PRINT         = 14;
    int MENU_HOME_REGULARIZACAO = 15;

    // Menu Type on CaixaResponse Window
    int MENU_CASHIER_SUP     = 1;
    int MENU_CASHIER_SANG    = 2;
    int MENU_CASHIER_CLOSE   = 3;
    int MENU_CASHIER_RESUME  = 4;
    int MENU_CASHIER_CONSULT = 5;
    int MENU_CASHIER_CLOSED  = 6;

    enum Method {
        DEBIT,
        CREDIT,
        CASH,
        NFC,
        WALLET
    }
}