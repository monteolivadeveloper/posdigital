
package br.com.estapar.posdigital.model.sync.user.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Agente {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ativo")
    @Expose
    private Boolean ativo;
    @SerializedName("registro")
    @Expose
    private String registro;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }
}