
package br.com.estapar.posdigital.model.sync.cashier.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Caixa {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("equipamento")
    @Expose
    private Integer equipamento;
    @SerializedName("estado")
    @Expose
    private String estado;
    @SerializedName("saldo")
    @Expose
    private Double saldo;
    @SerializedName("totalTransacoesDesdeAbertura")
    @Expose
    private Integer totalTransacoesDesdeAbertura;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Integer equipamento) {
        this.equipamento = equipamento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Integer getTotalTransacoesDesdeAbertura() {
        return totalTransacoesDesdeAbertura;
    }

    public void setTotalTransacoesDesdeAbertura(Integer totalTransacoesDesdeAbertura) {
        this.totalTransacoesDesdeAbertura = totalTransacoesDesdeAbertura;
    }
}