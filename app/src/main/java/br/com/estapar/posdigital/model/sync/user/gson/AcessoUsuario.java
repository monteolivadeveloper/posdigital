
package br.com.estapar.posdigital.model.sync.user.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcessoUsuario {
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("ultimoLogin")
    @Expose
    private String ultimoLogin;
    @SerializedName("expiracao")
    @Expose
    private String expiracao;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUltimoLogin() {
        return ultimoLogin;
    }

    public void setUltimoLogin(String ultimoLogin) {
        this.ultimoLogin = ultimoLogin;
    }

    public String getExpiracao() {
        return expiracao;
    }

    public void setExpiracao(String expiracao) {
        this.expiracao = expiracao;
    }
}