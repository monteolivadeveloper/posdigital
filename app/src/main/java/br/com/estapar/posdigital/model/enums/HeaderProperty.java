package br.com.estapar.posdigital.model.enums;

public enum HeaderProperty {
    ACCEPT("Accept"),
    CONTENT_TYPE("Content-Type");

    private String name;

    HeaderProperty(String name) { this.name = name; }

    public String propertyName() { return this.name; }
}