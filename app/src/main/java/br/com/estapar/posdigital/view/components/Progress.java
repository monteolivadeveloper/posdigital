package br.com.estapar.posdigital.view.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.R;

public class Progress {
    private TextView txtProgress;
    private View popupView;
    private PopupWindow popupWindow;

    /**
     * Constructor
     *
     * @param context
     */
    Progress(Context context) { init(context); }

    @SuppressLint("InflateParams")
    private void init(Context context) {
        AppCompatActivity activity = (AppCompatActivity) context;

        // pega o inflater
        LayoutInflater inflater = LayoutInflater.from(activity);

        // pega a View
        popupView = inflater.inflate(R.layout.progress, null);

        txtProgress = popupView.findViewById(R.id.txtProgress);
        txtProgress.setText(R.string.progress_wait);

        int width  = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        popupWindow = new PopupWindow(popupView, width, height, true);
    }

    public void show() { popupWindow.showAtLocation(popupView, Gravity.TOP, 0, 0); }
    public void hide() { popupWindow.dismiss(); }

    public void setText(@NonNull int message)    { txtProgress.setText(message); }
    public void setText(@NonNull String message) { txtProgress.setText(message); }

    public void setTextDefault() { txtProgress.setText(R.string.progress_wait); }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Progress build() {
            if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            return new Progress(context);
        }
    }
}