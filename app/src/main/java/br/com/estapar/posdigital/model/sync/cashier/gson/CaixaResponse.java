
package br.com.estapar.posdigital.model.sync.cashier.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaixaResponse {
    @SerializedName("caixa")
    @Expose
    private Caixa caixa;
    @SerializedName("codigoRetorno")
    @Expose
    private Integer codigoRetorno;
    @SerializedName("descricaoRetorno")
    @Expose
    private String descricaoRetorno;

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getDescricaoRetorno() {
        return descricaoRetorno;
    }

    public void setDescricaoRetorno(String descricaoRetorno) {
        this.descricaoRetorno = descricaoRetorno;
    }

}
