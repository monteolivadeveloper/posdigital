
package br.com.estapar.posdigital.model.sync.filial.gson;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filial {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("descricao")
    @Expose
    private String descricao;
    @SerializedName("operacaoOffline")
    @Expose
    private Boolean operacaoOffline;
    @SerializedName("obrigatorioVenda")
    @Expose
    private ObrigatorioVenda obrigatorioVenda;
    @SerializedName("setores")
    @Expose
    private List<Setor> setores = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getOperacaoOffline() {
        return operacaoOffline;
    }

    public void setOperacaoOffline(Boolean operacaoOffline) {
        this.operacaoOffline = operacaoOffline;
    }

    public ObrigatorioVenda getObrigatorioVenda() {
        return obrigatorioVenda;
    }

    public void setObrigatorioVenda(ObrigatorioVenda obrigatorioVenda) {
        this.obrigatorioVenda = obrigatorioVenda;
    }

    public List<Setor> getSetores() {
        return setores;
    }

    public void setSetores(List<Setor> setores) {
        this.setores = setores;
    }
}