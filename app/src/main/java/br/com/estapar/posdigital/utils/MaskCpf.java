package br.com.estapar.posdigital.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class MaskCpf implements TextWatcher {
    private EditText campo;
    private String mask      = "###.###.###-##";
    private String oldString = "";

    /**
     * Constructor
     *
     * @param campo
     */
    public MaskCpf(EditText campo) {
        super();

        // seta o campo
        this.campo = campo;
    }

    // inicia o Updating
    private boolean isUpdating = false;
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String newString = unmask(s.toString());
        if (newString.length() > unmask(mask).length()) {
            if (mask.length() == 1) {
                newString = newString.substring(0, newString.length() - 1);
            } else if (newString.length() > unmask(mask).length()) {
                newString = newString.substring(0, newString.length() - 1);
            }
        }

        if (isUpdating) {
            oldString = newString;
            isUpdating = false;
            return;
        }
        String maskedString;
        if (newString.length() <= unmask(mask).length() && newString.length() >= oldString.length()) {
            maskedString = applyMask(newString, mask);
        }
        else {
            maskedString = s.toString();
        }
        oldString = maskedString;
        isUpdating = true;
        campo.setText(maskedString);
        campo.setSelection(maskedString.length());
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private String applyMask(String oldString, String mask) {
        for (int i = 0; i < oldString.length(); i++) {
            if (mask.charAt(i) != '#') {
                if (mask.charAt(i) == '*') {
                    StringBuilder stringBuilder = new StringBuilder(oldString);
                    stringBuilder.setCharAt(i, mask.charAt(i));
                    oldString = stringBuilder.toString();
                } else {
                    oldString = new StringBuilder(oldString).insert(i, mask.charAt(i)).toString();
                }
            }
        }
        return oldString;
    }

     /**
     * Método resposnável por tirar uma mascara de um texto.
      *
     * @param string String string, texto com mascara.
     * @return String texto sem a mascara.
     */
    public static String unmask(String string) {
        return string.replaceAll("[.]", "").replaceAll("[-]", "")
                .replaceAll("[/]", "").replaceAll("[(]", "")
                .replaceAll("[)]", "")
                .replaceAll(" ", "");
    }
}