
package br.com.estapar.posdigital.model.sync.user.gson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Usuario {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("nome")
    @Expose
    private String nome;
    @SerializedName("tipoAcesso")
    @Expose
    private String tipoAcesso;
    @SerializedName("estado")
    @Expose
    private String estado;
    @SerializedName("equipamentos")
    @Expose
    private List<Integer> equipamentos = null;
    @SerializedName("filial")
    @Expose
    private Integer filial;
    @SerializedName("acessoUsuario")
    @Expose
    private AcessoUsuario acessoUsuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipoAcesso() {
        return tipoAcesso;
    }

    public void setTipoAcesso(String tipoAcesso) {
        this.tipoAcesso = tipoAcesso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Integer> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(List<Integer> equipamentos) {
        this.equipamentos = equipamentos;
    }

    public Integer getFilial() {
        return filial;
    }

    public void setFilial(Integer filial) {
        this.filial = filial;
    }

    public AcessoUsuario getAcessoUsuario() {
        return acessoUsuario;
    }

    public void setAcessoUsuario(AcessoUsuario acessoUsuario) {
        this.acessoUsuario = acessoUsuario;
    }
}
