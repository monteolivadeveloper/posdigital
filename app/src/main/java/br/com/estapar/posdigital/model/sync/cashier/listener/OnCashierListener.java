package br.com.estapar.posdigital.model.sync.cashier.listener;

public interface OnCashierListener {
    void openSuccess();
    void openError(String message);
    void openCashierPage();
}