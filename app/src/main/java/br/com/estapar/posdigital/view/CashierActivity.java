package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.os.Handler;
import android.view.KeyEvent;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.model.sync.cashier.listener.OnCashierListener;
import br.com.estapar.posdigital.model.sync.cashier.CashierModel;
import br.com.estapar.posdigital.utils.Constantes;
import br.com.estapar.posdigital.utils.Utils;

@EActivity(R.layout.activity_cashier)
@Fullscreen
public class CashierActivity extends DefaultActivity {
    @ViewById(R.id.txtCashierBalance)
    protected TextView balance;

    private CashierModel model;

    @App
    protected PosDigitalApplication app;

    private int MENU_TYPE = 0;

    @AfterViews
    protected void afterViews() {
        model = new CashierModel.Builder()
                .context(this)
                .application(app)
                .listener(cashierListener)
                .build();
    }

    @Override
    protected void resume() {
        new Handler().postDelayed(() -> {
            progress();
            MENU_TYPE = Constantes.MENU_CASHIER_CONSULT;
            model.consultarCaixa();
        }, 100);
    }

    @Click(R.id.btnCashierOpen)
    protected void abertura() {
        if (!app.isCashierOpen()) {
            startActivity(new Intent(this, CashierOpenActivity_.class));
            finish();
        }
        else {
            msgError(R.string.cashier_opened_error);
        }
    }

    @Click(R.id.btnCashierSup)
    protected void suprimento() {
        MENU_TYPE = Constantes.MENU_CASHIER_SUP;
        model.payment();
    }

    @Click(R.id.btnCashierSang)
    protected void sangria() {
        MENU_TYPE = Constantes.MENU_CASHIER_SANG;
        model.payment();
    }

    @Click(R.id.btnCashierClose)
    protected void fechamento() {
        MENU_TYPE = Constantes.MENU_CASHIER_CLOSE;
        model.payment();
    }

    @Click(R.id.btnCashierResu)
    protected void resumoDiario() {
        MENU_TYPE = Constantes.MENU_CASHIER_RESUME;
        model.payment();
    }

    private void closeCashier() {
        msgConfirm(R.string.cashier_close, () -> actClose());
    }

    private void actClose() {
        MENU_TYPE = Constantes.MENU_CASHIER_CLOSED;
        progress(R.string.cashier_closing);
        model.fecharCaixa();
    }

    /**
     * Method to back Home
     */
    private void back() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    /**
     * Evento KeyDown
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { back(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /*****************************************************************************
     * CallBack (listener)
     *****************************************************************************/
    private OnCashierListener cashierListener = new OnCashierListener() {
        @Override
        public void openSuccess() {
            switch (MENU_TYPE) {
                case Constantes.MENU_CASHIER_SUP:
                    startActivity(new Intent(getBaseContext(), SuprimentoActivity_.class));
                    finish();
                    break;
                case Constantes.MENU_CASHIER_SANG:
                    startActivity(new Intent(getBaseContext(), SangriaActivity_.class));
                    finish();
                    break;
                case Constantes.MENU_CASHIER_CLOSE:
                    closeCashier();
                    break;
                case Constantes.MENU_CASHIER_RESUME:
                    break;
                case Constantes.MENU_CASHIER_CLOSED:
                    msgSuccess(R.string.cashier_closed, () -> {});
                    break;
                case Constantes.MENU_CASHIER_CONSULT:
                    double saldo = 0.00;

                    if (app.getCaixaResponse() != null) {
                        saldo = app.getCaixaResponse().getCaixa().getSaldo();
                    }

                    String valueBalance = Utils.setFormatDecimal(saldo);
                    String txtBalance = getString(R.string.txt_balance);
                    balance.setText(txtBalance + " " + valueBalance);

                    hideMsg();
                    break;
            }
        }

        @Override
        public void openError(String message) {}

        @Override
        public void openCashierPage() { abertura(); }
    };
}