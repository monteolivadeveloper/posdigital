package br.com.estapar.posdigital.view.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnMsgCallback;

public class ActionMsg {
    private OnMsgCallback listener;
    private TextView txtBox;
    private TextView txtProgress;
    private View popupView;
    private PopupWindow popupWindow;
    private Button btnOk;
    private Button btnMsgCancel;
    private LinearLayout actMsgMessage;
    private LinearLayout actMsgProgress;

    /**
     * Constructor
     *
     * @param context
     */
    ActionMsg(Context context) { init(context); }

    @SuppressLint("InflateParams")
    public void init(Context context) {
        AppCompatActivity activity = (AppCompatActivity) context;

        // pega o inflater
        LayoutInflater inflater = LayoutInflater.from(activity);

        popupView      = inflater.inflate(R.layout.action_msg, null);
        txtBox         = popupView.findViewById(R.id.txtMsgBox);
        actMsgMessage  = popupView.findViewById(R.id.actMsgMessage);
        actMsgProgress = popupView.findViewById(R.id.actMsgProgress);
        txtProgress    = popupView.findViewById(R.id.txtProgress);

        setProgressTextDefault();
        hideProgress();

        btnOk = popupView.findViewById(R.id.btnMsgOk);
        btnOk.setOnClickListener(view1 -> {
            hide();
            if (listener != null) { listener.onMsgSuccess(); }
        });

        btnMsgCancel = popupView.findViewById(R.id.btnMsgCancel);
        btnMsgCancel.setOnClickListener(view1 -> hide());

        int width  = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        popupWindow = null;
        popupWindow = new PopupWindow(popupView, width, height, true);
    }

    public void show() { popupWindow.showAtLocation(popupView, Gravity.TOP, 0, 0); }
    public void hide() { popupWindow.dismiss(); }

    public void setText(@NonNull String message) { txtBox.setText(message); }
    public void setText(@NonNull int message)    { txtBox.setText(message); }

    public void setOkText(@NonNull int message)    { btnOk.setText(message); }
    public void setOkText(@NonNull String message) { btnOk.setText(message); }

    public void showBtnOk() { btnOk.setVisibility(View.VISIBLE); }
    public void hideBtnOk() { btnOk.setVisibility(View.GONE);    }

    public void showBtnCancel() { btnMsgCancel.setVisibility(View.VISIBLE);   }
    public void hideBtnCancel() { btnMsgCancel.setVisibility(View.INVISIBLE); }

    public void showMessage() { actMsgMessage.setVisibility(View.VISIBLE); }
    public void hideMessage() { actMsgMessage.setVisibility(View.GONE);    }

    public void showProgress() { actMsgProgress.setVisibility(View.VISIBLE); }
    public void hideProgress() { actMsgProgress.setVisibility(View.GONE);    }

    public void setProgressText(@NonNull int message)    { txtProgress.setText(message); }
    public void setProgressText(@NonNull String message) { txtProgress.setText(message); }

    public void setProgressTextDefault() { txtProgress.setText(R.string.progress_wait); }

    public void setOnMsgCallback(@NonNull OnMsgCallback listener) { this.listener = listener; }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public ActionMsg build() {
            if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            return new ActionMsg(context);
        }
    }
}