package br.com.estapar.posdigital.listener;

public interface OnBtnKeyListener {
    void onKeyClick(String value);
}