package br.com.estapar.posdigital.listener;

import androidx.annotation.NonNull;

import br.com.estapar.posdigital.bean.InfoBean;

public interface OnInfoListener {
    void onInfoLoad(@NonNull InfoBean infoBean);
}
