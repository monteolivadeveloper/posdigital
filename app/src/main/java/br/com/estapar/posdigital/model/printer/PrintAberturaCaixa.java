package br.com.estapar.posdigital.model.printer;

import androidx.annotation.NonNull;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.utils.Utils;

public class PrintAberturaCaixa {
    private PrintModel printer;
    private PosDigitalApplication app;
    private double value;

    PrintAberturaCaixa(@NonNull double value,
                       @NonNull PosDigitalApplication app) {
        this.value    = value;
        this.app      = app;
        this.printer  = new PrintModel();

        printRecibo();
    }

    private void printRecibo() {
        String dateNow = Utils.getNow();
        String saldoEx = Utils.setFormatDecimal(value);

        printer.addTitle("ESTAPAR");
        printer.addSubTitle("Estacionamentos");
        printer.addLines(1);

        printer.addTitle("ABERTURA DE CAIXA");
        printer.addTextLeft("");
        printer.addTextLeft("PDV N: " + app.IMEI);
        printer.addTextLeft("Data Abertura: " + dateNow);
        printer.addLines(1);

        printer.addTitleLeft("VALOR R$: " + saldoEx);
        printer.print();
    }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private PosDigitalApplication app;
        private double value;

        public Builder value(double value) {
            this.value = value;
            return this;
        }

        public Builder application(PosDigitalApplication app) {
            this.app = app;
            return this;
        }

        public PrintAberturaCaixa build() {
            if (app == null) {
                throw new IllegalStateException("Application required.");
            }
            return new PrintAberturaCaixa(value, app);
        }
    }
}