package br.com.estapar.posdigital.model.printer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import com.getnet.posdigital.PosDigital;
import com.getnet.posdigital.printer.AlignMode;
import com.getnet.posdigital.printer.FontFormat;
import com.getnet.posdigital.printer.IPrinterCallback;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnCallbackListener;
import br.com.estapar.posdigital.model.mifare.MifareType;

public class PrintModel {
    private PosDigital app;
    private OnCallbackListener listener;

    /**
     * Constructor
     */
    public PrintModel() {
        try {
            app = PosDigital.getInstance();
            app.getPrinter().init();
            app.getPrinter().setGray(5);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addLogo(Context context) {
        try {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_logo_print);
            app.getPrinter().addImageBitmap(AlignMode.CENTER, bitmap);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public void addTextLeft(@NonNull String text) {
        try {
            app.getPrinter().defineFontFormat(FontFormat.MEDIUM);
            app.getPrinter().addText(AlignMode.LEFT, text);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public void addTextCenter(@NonNull String text) {
        try {
            app.getPrinter().defineFontFormat(FontFormat.MEDIUM);
            app.getPrinter().addText(AlignMode.CENTER, text);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addTitle(@NonNull String text) {
        try {
            app.getPrinter().defineFontFormat(FontFormat.LARGE);
            app.getPrinter().addText(AlignMode.CENTER, text);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addTitleLeft(@NonNull String text) {
        try {
            app.getPrinter().defineFontFormat(FontFormat.LARGE);
            app.getPrinter().addText(AlignMode.LEFT, text);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addSubTitle(@NonNull String text) {
        try {
            app.getPrinter().defineFontFormat(FontFormat.SMALL);
            app.getPrinter().addText(AlignMode.CENTER, text);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void print() {
        if (!app.isInitiated()) { return; }
        try {
            addLines(6);
            app.getPrinter().print(new IPrinterCallback.Stub() {
                @Override
                public void onSuccess() {
                    if (listener != null) { listener.onCallbackSuccess(MifareType.TYPE_FINISH); }
                }

                @Override
                public void onError(int i) {
                }
            });
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void addLines(int qtde) {
        try {
            String txt = "";
            for (int ii = 0; ii < qtde; ii++) { txt += "\n\r"; }
            app.getPrinter().addText(AlignMode.LEFT, txt);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setOnCallbackListener(OnCallbackListener listener) { this.listener = listener; }
}