
package br.com.estapar.posdigital.model.sync.filial.gson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Setor {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("tempoMaximoPermitido")
    @Expose
    private Integer tempoMaximoPermitido;
    @SerializedName("tarifas")
    @Expose
    private List<Tarifa> tarifas = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getTempoMaximoPermitido() {
        return tempoMaximoPermitido;
    }

    public void setTempoMaximoPermitido(Integer tempoMaximoPermitido) {
        this.tempoMaximoPermitido = tempoMaximoPermitido;
    }

    public List<Tarifa> getTarifas() {
        return tarifas;
    }

    public void setTarifas(List<Tarifa> tarifas) {
        this.tarifas = tarifas;
    }

}
