package br.com.estapar.posdigital.model.sync.listener;

public interface OnSyncListener {
    void syncSuccess();
    void syncError(String message);
}