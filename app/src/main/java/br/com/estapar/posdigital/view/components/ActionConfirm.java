package br.com.estapar.posdigital.view.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnMsgCallback;

public class ActionConfirm {
    private OnMsgCallback listener;
    private TextView txtBox;
    private View popupView;
    private PopupWindow popupWindow;
    private Button btnOk;

    /**
     * Constructor
     *
     * @param context
     */
    ActionConfirm(Context context) { init(context); }

    @SuppressLint("InflateParams")
    private void init(Context context) {
        AppCompatActivity activity = (AppCompatActivity) context;

        // pega o inflater
        LayoutInflater inflater = LayoutInflater.from(activity);

        // pega a View
        popupView = inflater.inflate(R.layout.action_confirm, null);

        txtBox = popupView.findViewById(R.id.txtMsgBox);

        btnOk = popupView.findViewById(R.id.btnMsgOk);
        btnOk.setOnClickListener(view1 -> {
            hide();
            if (listener != null) { listener.onMsgSuccess(); }
        });

        Button btnMsgCancel = popupView.findViewById(R.id.btnMsgCancel);
        btnMsgCancel.setOnClickListener(view1 -> hide());

        int width  = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        popupWindow = new PopupWindow(popupView, width, height, true);
    }

    public void show() { popupWindow.showAtLocation(popupView, Gravity.TOP, 0, 0); }
    public void hide() { popupWindow.dismiss(); }

    public void setText(String message) { txtBox.setText(message); }
    public void setText(int message)    { txtBox.setText(message); }

    public void setOkText(int message)    { btnOk.setText(message); }
    public void setOkText(String message) { btnOk.setText(message); }

    public void setOnMsgCallback(OnMsgCallback listener) { this.listener = listener; }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public ActionConfirm build() {
            if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            return new ActionConfirm(context);
        }
    }
}