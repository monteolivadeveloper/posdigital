package br.com.estapar.posdigital.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnValuesListener;

public class BtnValues extends LinearLayout {
    private String retTarifa;
    private int retMinutes;
    private String retValue;
    private Button btn;
    private OnValuesListener listener;

    public BtnValues(Context context, AttributeSet attrs) { super(context, attrs); init(context, attrs); }

    private void init(Context context, AttributeSet attrs) {
        // seta o Background
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        // pega o inflater
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // pega a View
        View view = inflater.inflate(R.layout.btn_values, this);

        // pega os atributos
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BtnValues);

        // get parameters
        retTarifa  = a.getString(R.styleable.BtnValues_tarifa);
        retMinutes = a.getInteger(R.styleable.BtnValues_minutes, 30);
        retValue   = a.getString(R.styleable.BtnValues_value);

        final String text = retTarifa + "\nR$ " + retValue;

        // recycle
        a.recycle();

        // get Text
        btn = view.findViewById(R.id.btnValues);
        btn.setText(text);
        btn.setOnClickListener(view1 -> {
            if (listener != null) { listener.onClick(retTarifa, retMinutes, retValue); }
        });
    }

    public int getRetMinutes()   { return retMinutes; }
    public String getRetTarifa() { return retTarifa;  }
    public String getRetValue()  { return retValue;   }

    public void setSelected(@NonNull boolean isSelect) { btn.setActivated(isSelect); }

    public void setOnValuesListener(OnValuesListener listener) { this.listener = listener; }
}