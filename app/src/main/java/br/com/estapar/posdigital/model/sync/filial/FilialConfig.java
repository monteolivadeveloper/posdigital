package br.com.estapar.posdigital.model.sync.filial;

import android.content.Context;

import androidx.annotation.NonNull;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import httpaction.HttpCallBack;
import httpaction.HttpResponse;

import br.com.estapar.posdigital.BuildConfig;
import br.com.estapar.posdigital.model.enums.Endpoints;
import br.com.estapar.posdigital.model.sync.SyncType;
import br.com.estapar.posdigital.utils.http.RestCallback;
import br.com.estapar.posdigital.utils.http.RestService;

public class FilialConfig {
    private String BASE_URL = BuildConfig.BASE_URL;
    private RestService service;
    private RestCallback callBack;

    public FilialConfig(Context context) {
        service = new RestService.Builder()
                .baseUrl(BASE_URL)
                .context(context)
                .build();
    }

    public void tarifa(@NonNull int idFilial,
                        @NonNull String userLogin,
                        @NonNull String imei,
                        @NonNull String token) {
        String wsEndpoint = Endpoints.FILIAL_CONSULTAR_TARIFAS.endpoint();
        String uri        = MessageFormat.format(wsEndpoint, idFilial);

        Map<String, String> requestParam = new HashMap<>();
        requestParam.put("usuario", userLogin);
        requestParam.put("dispositivo", imei);
        requestParam.put("token", token);

        service.callWsGet(uri,
                requestParam,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse httpResponse) {
                        if (callBack != null) {
                            callBack.onResponse(httpResponse, SyncType.FILIAL_TARIFA);
                        }
                    }

                    @Override
                    public void onError(HttpResponse httpResponse) {
                        if (callBack != null) { callBack.onError(httpResponse); }
                    }
                });
    }







    public void setCallBack(RestCallback callBack) { this.callBack = callBack; }
}