package br.com.estapar.posdigital.listener;

import androidx.annotation.NonNull;

public interface OnCallbackListener {
    void onCallbackSuccess(@NonNull int type);
    void onCallbackError(@NonNull String message);
}