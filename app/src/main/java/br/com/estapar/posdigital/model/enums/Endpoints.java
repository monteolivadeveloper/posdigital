package br.com.estapar.posdigital.model.enums;

public enum Endpoints {
    USUARIO_AUTENTICAR("/pdvs/usuarios/{0}/autenticar"),
    USUARIO_CONSULTAR("/pdvs/usuarios/{0}"),
    USUARIO_REDEFINIR_SENHA("/pdvs/usuarios/{0}/redefinirsenha"),
    USUARIO_RECUPERAR_SENHA("/pdvs/usuarios/{0}/recuperarsenha"),
    CAIXA_ATUALIZAR("/pdvs/caixas/{0}/transacoes"),
    CAIXA_CONSULTAR("/pdvs/caixas/{0}"),
    CAIXA_CONSULTAR_MOVIMENTACOES("/pdvs/caixas/{0}/transacoes/{1}"),
    TIQUETE_CONSULTAR("/pdvs/tiquetes/{0}"),
    TIQUETE_CONSULTAR_ULTIMOS("/pdvs/tiquetes/ultimos"),
    FILIAL_CONSULTAR_TARIFAS("/pdvs/filiais/{0}/tarifas"),
    FILIAL_CONSULTAR_VAGAS("/pdvs/filiais/{0}/vagas");

    private String endpoint;

    Endpoints(String endpoint) {
        this.endpoint = endpoint;
    }

    public String endpoint() { return this.endpoint; }
}