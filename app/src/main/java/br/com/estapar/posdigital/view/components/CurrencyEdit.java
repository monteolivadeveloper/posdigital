package br.com.estapar.posdigital.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.utils.MaskDecimal;
import br.com.estapar.posdigital.view.keyboard.KeyBoardNumber;

public class CurrencyEdit extends LinearLayout {
    private EditText currencyEdit;
    private KeyBoardNumber keyboard;

    /**
     * Constructor
     *
     * @param context
     * @param attrs
     */
    public CurrencyEdit(Context context, AttributeSet attrs) { super(context, attrs); init(context); }
    public CurrencyEdit(Context context)                     { super(context);        init(context); }

    private void init(Context context) {
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.currency_edit, this);

        currencyEdit = view.findViewById(R.id.currencyEdit);
        currencyEdit.addTextChangedListener(new MaskDecimal(currencyEdit));

        keyboard = view.findViewById(R.id.keyboardNumber);
        keyboard.setEdit(currencyEdit);
    }

    public String getValue() {
        String value      = currencyEdit.getEditableText().toString().trim();
        String[] retValue = value.split(" ");
        return retValue[1];
    }
}