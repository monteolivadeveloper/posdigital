package br.com.estapar.posdigital;

import android.app.Application;
import android.util.Log;

import com.getnet.posdigital.PosDigital;

import org.androidannotations.annotations.EApplication;

import br.com.estapar.posdigital.bean.InfoBean;
import br.com.estapar.posdigital.model.InfoModel;
import br.com.estapar.posdigital.model.sync.cashier.gson.CaixaResponse;
import br.com.estapar.posdigital.model.sync.filial.gson.FilialResponse;
import br.com.estapar.posdigital.model.sync.user.gson.UsuarioResponse;

@EApplication
public class PosDigitalApplication extends Application {
    private static final String TAG = "POS";
    private InfoBean infoDataBean;
    private CaixaResponse caixaResponse;
    private UsuarioResponse usuarioResponse;
    private FilialResponse filialResponse;

    // imei from device
    public String IMEI;

    private boolean isCashierOpen;

    @Override
    public void onCreate() {
        super.onCreate();

        // register SDK Getnet
        PosDigital.register(getApplicationContext(), bindCallback);

        IMEI = "868557039285804"; //Device.getImei(this);
        isCashierOpen = false;
    }

    private void getLocalInfo() {
        new InfoModel().getInfo(infoBean -> infoDataBean = infoBean);
    }

    private PosDigital.BindCallback bindCallback = new PosDigital.BindCallback() {
        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onConnected() {
            Log.d("MIFARE", "PosDigital connected!");
            getLocalInfo();
        }

        @Override
        public void onDisconnected() {

        }
    };

    public InfoBean getInfo() { return infoDataBean; }

    public boolean isCashierOpen() { return isCashierOpen; }
    public void setCashierOpen(boolean isCashierOpen) { this.isCashierOpen = isCashierOpen; }

    public CaixaResponse getCaixaResponse() { return caixaResponse; }
    public void setCaixaResponse(CaixaResponse caixaResponse) {
        this.caixaResponse = caixaResponse;
    }

    public UsuarioResponse getUsuarioResponse() { return usuarioResponse; }
    public void setUsuarioResponse(UsuarioResponse usuarioResponse) {
        this.usuarioResponse = usuarioResponse;
    }

    public FilialResponse getFilialResponse() { return filialResponse; }
    public void setFilialResponse(FilialResponse filialResponse) {
        this.filialResponse = filialResponse;
    }
}