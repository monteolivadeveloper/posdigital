package br.com.estapar.posdigital.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnCallbackListener;
import br.com.estapar.posdigital.model.mifare.MifareBoxNew;
import br.com.estapar.posdigital.model.mifare.MifareType;
import br.com.estapar.posdigital.model.mifare.MifareUtils;
import br.com.estapar.posdigital.view.components.CurrencyEdit;

@EActivity(R.layout.activity_mifare_credit)
@Fullscreen
public class MifareCreditActivity extends AppCompatActivity implements OnCallbackListener {
    @ViewById(R.id.txtTitleCredit)
    protected TextView txtTitleCredit;
    @ViewById(R.id.editValue)
    protected CurrencyEdit editValue;

    private String CREDIT_OPTION;
    private MifareBoxNew mifareBox;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        Bundle bundle = getIntent().getExtras();

        CREDIT_OPTION = bundle.getString("CREDIT_OPTION");

        txtTitleCredit.setText(bundle.getString("CREDIT_TITLE"));

        mifareBox = MifareBoxNew.getInstance(this);
        mifareBox.initMifare();
        mifareBox.setOnCallbackListener(this);
    }

    @Click(R.id.btnNfcSave)
    protected void save() {
        double saveValue = MifareUtils.getDoubleValue(editValue.getValue());
        try {
            mifareBox.setDataUser(saveValue);

            if      (CREDIT_OPTION.equals("ACTIVATE")) { mifareBox.blankCard(); }
            else if (CREDIT_OPTION.equals("CREDIT"))   { mifareBox.incrementValue(); }
        }
        catch (RemoteException re) {}
    }

    private void back() {
        startActivity(new Intent(this, MifareActivity_.class));
        finish();
    }

    private void menu() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    /**
     * Evento KeyDown
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { menu(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /**************************************************************************
     * Methods for Mifare CallBack
     **************************************************************************/
    @Override
    public void onCallbackSuccess(@NonNull int type) {
        if (type == MifareType.TYPE_FINISH) { back(); }
    }

    @Override
    public void onCallbackError(@NonNull String message) {}
}