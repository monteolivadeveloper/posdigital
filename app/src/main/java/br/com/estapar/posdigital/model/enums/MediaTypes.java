package br.com.estapar.posdigital.model.enums;

public enum MediaTypes {
    APPLICATION_JSON("application/json"),
    APPLICATION_XML("application/xml");

    private String type;

    MediaTypes(String type) { this.type = type; }

    public String type() { return this.type; }
}