package br.com.estapar.posdigital.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.KeyEvent;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;

import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.listener.OnCallbackListener;
import br.com.estapar.posdigital.model.mifare.MifareBoxNew;
import br.com.estapar.posdigital.model.mifare.MifareType;

@EActivity(R.layout.activity_mifare)
@Fullscreen
public class MifareActivity extends AppCompatActivity implements OnCallbackListener {
    private MifareBoxNew mifareBox;

    @App
    protected PosDigitalApplication app;

    @AfterViews
    protected void afterViews() {
        mifareBox = MifareBoxNew.getInstance(this);
        mifareBox.initMifare();
        mifareBox.setOnCallbackListener(this);
    }

    @Click(R.id.btnNfcAtivar)
    protected void btnActivite() {
        Bundle bundle = new Bundle();
        bundle.putString("CREDIT_TITLE", getString(R.string.nfc_option1));
        bundle.putString("CREDIT_OPTION", "ACTIVATE");

        Intent intent = new Intent(this, MifareCreditActivity_.class);
        intent.putExtras(bundle);

        startActivity(intent);
        finish();
    }

    @Click(R.id.btnNfcCredit)
    protected void btnCredit() {
        Bundle bundle = new Bundle();
        bundle.putString("CREDIT_TITLE", getString(R.string.nfc_option3));
        bundle.putString("CREDIT_OPTION", "CREDIT");

        Intent intent = new Intent(this, MifareCreditActivity_.class);
        intent.putExtras(bundle);

        startActivity(intent);
        finish();
    }

    @Click(R.id.btnNfcBalance)
    protected void btnBalance() {
        try {
            mifareBox.balance();
        }
        catch (RemoteException re) {}
    }

    /**
     * Method to back Home
     */
    private void back() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if      (keyCode == KeyEvent.KEYCODE_BACK) { back(); return true; }
        else if (keyCode == KeyEvent.KEYCODE_MENU) { back(); return true; }
        else                                       { return super.onKeyDown(keyCode, event); }
    }

    /**************************************************************************
     * Methods for Mifare CallBack
     **************************************************************************/
    @Override
    public void onCallbackSuccess(@NonNull int type) {
        if (type == MifareType.TYPE_FINISH) {

        }
    }

    @Override
    public void onCallbackError(@NonNull String message) {}
}