package br.com.estapar.posdigital.utils;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import br.com.estapar.posdigital.BuildConfig;

public class Security {
    public static String encrypt(String plainText) {
        try {
            SecretKey secret_key = new SecretKeySpec(BuildConfig.KEY.getBytes(), BuildConfig.ALGORITM);

            Cipher cipher = Cipher.getInstance(BuildConfig.ALGORITM);
            cipher.init(Cipher.ENCRYPT_MODE, secret_key);

            byte[] data = cipher.doFinal(plainText.getBytes());

            return Convert.bytesToHex(data);
        }
        catch (GeneralSecurityException ge) {
            return null;
        }
    }

    public static String decrypt(String textEncrypted) {
        try {
            byte[] encryptedText = Convert.hexToBytes(textEncrypted);

            SecretKey secret_key = new SecretKeySpec(BuildConfig.KEY.getBytes(), BuildConfig.ALGORITM);

            Cipher cipher = Cipher.getInstance(BuildConfig.ALGORITM);
            cipher.init(Cipher.DECRYPT_MODE, secret_key);

            byte[] decrypted = cipher.doFinal(encryptedText);

            return new String(decrypted);
        }
        catch (GeneralSecurityException ge) {
            return "";
        }
    }
}