package br.com.estapar.posdigital.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.core.content.ContextCompat;

import br.com.estapar.posdigital.bean.WifiBean;

public class Device {
    /**
     * Method for return IMEI Number (Permission READ_PHONE_STATE Needs)
     *
     * @param context
     * @return
     */
    public static String getImei(Context context) {
        String[] perms = { android.Manifest.permission.READ_PHONE_STATE };

        try {
            // Telephony Manager
            final TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // init return
            String imeiNumber = "";

            if (ContextCompat.checkSelfPermission(context, perms[0]) == PackageManager.PERMISSION_GRANTED) {
                // verify if SDK 23
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    imeiNumber = manager.getDeviceId(0);
                }
                else {
                    imeiNumber = manager.getDeviceId();
                }
            }

            // return IMEI
            return imeiNumber;
        }
        catch (Exception e) { return ""; }
    }

    public static WifiBean getWifiInfo(Context ctx) {
        final String[] perms = { Manifest.permission.ACCESS_WIFI_STATE };
        final WifiBean wifi = new WifiBean();

        try {
            if (ContextCompat.checkSelfPermission(ctx, perms[0]) == PackageManager.PERMISSION_GRANTED) {
                // pega informacoes do WiFi
                final WifiInfo wifiInfo = ((WifiManager) ctx.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();

                // seta as informacoes
                wifi.setIpAddress(wifiInfo.getIpAddress());
                wifi.setMacAddress(wifiInfo.getMacAddress());
                wifi.setSSID(wifiInfo.getSSID());
            }
        }
        catch (Exception e) { }

        // retorna
        return wifi;
    }

    public static boolean isInternetConnect(Context ctx) {
        String[] perms = { Manifest.permission.ACCESS_NETWORK_STATE };
        try {
            if (ContextCompat.checkSelfPermission(ctx, perms[0]) == PackageManager.PERMISSION_GRANTED) {
                // pega a conexao
                final ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

                // pega a conexao ativa
                final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                // retorna se esta ou nao conectado
                return activeNetwork.isConnected();
            }
        }
        catch (Exception e) { return false; }

        return false;
    }

    public static boolean isGPS(Context context) {
        // pega op Location Manager
        final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );

        // verifica o GPS Provider
        final boolean isGpsOn = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // verifica o Network Provider
        final boolean isNetOn = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // retorna
        return (!isGpsOn && !isNetOn) ? false : true;
    }

    public static boolean isTablet(Context context) {
        // pega dados
        final TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        // retorno
        return (manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE);
    }
}