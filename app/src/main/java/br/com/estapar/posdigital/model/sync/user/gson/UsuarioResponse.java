
package br.com.estapar.posdigital.model.sync.user.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsuarioResponse {
    @SerializedName("agente")
    @Expose
    private Agente agente;
    @SerializedName("usuario")
    @Expose
    private Usuario usuario;
    @SerializedName("codigoRetorno")
    @Expose
    private Integer codigoRetorno;
    @SerializedName("descricaoRetorno")
    @Expose
    private String descricaoRetorno;

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(Integer codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public String getDescricaoRetorno() {
        return descricaoRetorno;
    }

    public void setDescricaoRetorno(String descricaoRetorno) {
        this.descricaoRetorno = descricaoRetorno;
    }
}