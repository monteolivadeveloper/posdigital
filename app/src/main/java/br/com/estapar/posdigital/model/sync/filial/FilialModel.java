package br.com.estapar.posdigital.model.sync.filial;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import httpaction.HttpResponse;

import br.com.estapar.posdigital.model.sync.filial.gson.FilialResponse;
import br.com.estapar.posdigital.PosDigitalApplication;
import br.com.estapar.posdigital.utils.http.RestCallback;
import br.com.estapar.posdigital.model.sync.SyncType;
import br.com.estapar.posdigital.model.sync.listener.OnSyncListener;

public class FilialModel {
    private Context context;
    private OnSyncListener listener;
    private PosDigitalApplication app;
    private FilialConfig config;
    private String token;
    private String login;
    private int idFilial;

    FilialModel(@NonNull Context context,
                @NonNull PosDigitalApplication app,
                @NonNull OnSyncListener listener) {
        this.context  = context;
        this.app      = app;
        this.listener = listener;
        this.token    = app.getUsuarioResponse().getUsuario().getAcessoUsuario().getToken();
        this.login    = app.getUsuarioResponse().getUsuario().getNome();
        this.idFilial = app.getUsuarioResponse().getUsuario().getFilial();

        this.config = new FilialConfig(context);
        this.config.setCallBack(callBack);
    }

    private void obterTarifas(HttpResponse response) { setResponse(response); }

    private void setResponse(HttpResponse response) {
        FilialResponse filial = new Gson().fromJson(response.getMessage(), FilialResponse.class);
        if (listener != null) {
            int codeHttp = response.getCodeHttp();
            if (codeHttp == 200 || codeHttp == 201) {
                app.setFilialResponse(filial);

                listener.syncSuccess();
            }
            else { listener.syncError(filial.getDescricaoRetorno()); }
        }
    }

    /**************************************************************************************
     * Callback
     **************************************************************************************/
    private RestCallback callBack = new RestCallback() {
        @Override
        public void onResponse(HttpResponse response, int type) {
            switch (type) {
                case SyncType.FILIAL_TARIFA:
                    obterTarifas(response);
                    break;
            }
        }

        @Override
        public void onError(HttpResponse httpResponse) {
            if (listener != null) { listener.syncError(httpResponse.getMessage()); }
        }
    };

    /**************************************************************************************
     * Actions
     **************************************************************************************/
    public void obterTarifasFilial() {
        this.config.tarifa(idFilial, login, app.IMEI, token);
    }

    /**************************************************************************************
     * Builder (Design Patterns)
     **************************************************************************************/
    public static class Builder {
        private Context context;
        private PosDigitalApplication app;
        private OnSyncListener listener;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder application(PosDigitalApplication app) {
            this.app = app;
            return this;
        }

        public Builder listener(OnSyncListener listener) {
            this.listener = listener;
            return this;
        }

        public FilialModel build() {
            if (app == null) {
                throw new IllegalStateException("Application required.");
            }
            else if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            else if (listener == null) {
                throw new IllegalStateException("Listener required.");
            }
            return new FilialModel(context, app, listener);
        }
    }
}