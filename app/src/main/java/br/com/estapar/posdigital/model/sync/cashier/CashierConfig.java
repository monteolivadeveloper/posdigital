package br.com.estapar.posdigital.model.sync.cashier;

import android.content.Context;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import httpaction.HttpCallBack;
import httpaction.HttpResponse;

import br.com.estapar.posdigital.model.enums.Endpoints;
import br.com.estapar.posdigital.BuildConfig;
import br.com.estapar.posdigital.utils.http.RestCallback;
import br.com.estapar.posdigital.utils.http.RestService;
import br.com.estapar.posdigital.model.sync.SyncType;

public class CashierConfig {
    private String BASE_URL = BuildConfig.BASE_URL;
    private RestService service;
    private RestCallback callBack;

    public CashierConfig(Context context) {
        service = new RestService.Builder()
                .baseUrl(BASE_URL)
                .context(context)
                .build();
    }

    public void atualizarCaixa(@NonNull int type,
                               @NonNull String userLogin,
                               @NonNull String imei,
                               @NonNull String token,
                               @NonNull double value) {

        String wsEndpoint = Endpoints.CAIXA_ATUALIZAR.endpoint();
        String uri        = MessageFormat.format(wsEndpoint, imei);

        Map<String, String> requestParam = new HashMap<>();
                            requestParam.put("usuario", userLogin);
                            requestParam.put("token", token);

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("codigoOperacao", type);
            jsonBody.put("valorOperacao", value);
        }
        catch (JSONException e) { e.printStackTrace(); }

        service.callWsPost(uri,
                requestParam,
                jsonBody,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse httpResponse) {
                        if (callBack != null) {
                            callBack.onResponse(httpResponse, type);
                        }
                    }

                    @Override
                    public void onError(HttpResponse httpResponse) {
                        if (callBack != null) { callBack.onError(httpResponse); }
                    }
                });
    }

    public void consultCashier(@NonNull String userLogin,
                               @NonNull String imei,
                               @NonNull String token) {
        String wsEndpoint = Endpoints.CAIXA_CONSULTAR.endpoint();
        String uri        = MessageFormat.format(wsEndpoint, imei);

        Map<String, String> requestParam = new HashMap<>();
        requestParam.put("usuario", userLogin);
        requestParam.put("token", token);

        service.callWsGet(uri,
                requestParam,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse httpResponse) {
                        if (callBack != null) {
                            callBack.onResponse(httpResponse, SyncType.CONSULTAR_CAIXA);
                        }
                    }

                    @Override
                    public void onError(HttpResponse httpResponse) {
                        if (callBack != null) { callBack.onError(httpResponse); }
                    }
                });
    }

    public void setCallBack(RestCallback callBack) { this.callBack = callBack; }
}