package br.com.estapar.posdigital.utils;

import java.text.NumberFormat;
import java.util.Locale;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class MaskDecimal implements TextWatcher {
    final EditText campo;

    private boolean isUpdating = false;
    private Locale locale      = new Locale("pt", "BR");

    // Pega a formatacao do sistema, se for brasil R$ se EUA US$
    private NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    /**
     * Constructor
     *
     * @param campo
     */
    public MaskDecimal(EditText campo) {
        super();

        // seta o campo
        this.campo = campo;
        setValue(campo.getEditableText().toString());
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Evita que o metodo seja executado varias vezes.
        // Se tirar ele entre em loop
        if (isUpdating) {
            isUpdating = false;
            return;
        }

        // seta com o true
        isUpdating = true;
        setValue(s);
    }

    private void setValue(CharSequence s) {
        String str = s.toString();
               str = umask(str);

        try {
            // Transformamos o numero que esta escrito no EditText em monetario.
            str = nf.format(Double.parseDouble(str) / 100);
            str = str.replace("R$", "R$ ");

            // coloca o texto no campo
            campo.setText(str);
            campo.setSelection(campo.getText().length());
        }
        catch (NumberFormatException e) { s = ""; }
    }

    /**
     * Method to clear old mask
     *
     * @param str
     * @return
     */
    private String umask(String str) {
        // Verifica se ja existe a mascara no texto.
        boolean hasMask = ((str.indexOf("R$") > -1 || str.indexOf("$") > -1) && (str.indexOf(".") > -1 || str.indexOf(",") > -1));

        // Verificamos se existe mascara
        if (hasMask) {
            // Retiramos a mascara.
            str = str.replaceAll("[R$]", "").replaceAll("[,]", "").replaceAll("[.]", "");
        }

        return str;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void afterTextChanged(Editable s) {}
}