package br.com.estapar.posdigital.bean;

import org.androidannotations.annotations.EBean;

import java.io.Serializable;

@EBean
public class PrintBean implements Serializable {
    private String nrPlaca;
    private String value;
    private String tarifa;
    private int minutes;
    private String formaPagto;

    public String getNrPlaca()    { return nrPlaca; }
    public String getValue()      { return value;   }
    public String getTarifa()     { return tarifa;  }
    public int getMinutes()       { return minutes; }
    public String getFormaPagto() { return formaPagto; }

    public void setNrPlaca(String nrPlaca)       { this.nrPlaca    = nrPlaca; }
    public void setValue(String value)           { this.value      = value;   }
    public void setTarifa(String tarifa)         { this.tarifa     = tarifa;  }
    public void setMinutes(int minutes)          { this.minutes    = minutes; }
    public void setFormaPagto(String formaPagto) { this.formaPagto = formaPagto;}
}