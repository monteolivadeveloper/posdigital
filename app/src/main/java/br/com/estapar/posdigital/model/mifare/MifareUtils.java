package br.com.estapar.posdigital.model.mifare;

public class MifareUtils {
    /**
     * Metodo que converte de ByteArray para Hextring
     *
     * @param bytes
     * @return
     */
    public static String bytes2HexString(byte[] bytes) {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] ret = new char[bytes.length << 1];
        for (int i = 0, j = 0; i < bytes.length; i++) {
            ret[j++] = hexDigits[bytes[i] >>> 4 & 0x0f];
            ret[j++] = hexDigits[bytes[i] & 0x0f];
        }
        return new String(ret);
    }

    public static double getDoubleValue(String value) {
        String dataValue = (value.contains("0")) ? value.replace(",", ".") : "0.00";
        double result = Double.valueOf(dataValue);
        return  Math.round(result);
    }
}