package br.com.estapar.posdigital.model.enums;

public enum ARG {
    RESULT("result"),
    RESULT_DETAILS("resultDetails"),
    AMOUNT("amount"),
    TYPE("type"),
    INPUT_TYPE("inputType"),
    INSTALLMENTS("installments"),
    NSU("nsu"),
    BRAND("brand");

    private String result;

    ARG(String result) { this.result = result; }

    public String get() { return this.result; }
}