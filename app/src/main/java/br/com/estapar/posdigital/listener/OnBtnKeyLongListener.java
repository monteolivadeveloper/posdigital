package br.com.estapar.posdigital.listener;

public interface OnBtnKeyLongListener {
    void onKeyLongClick();
}