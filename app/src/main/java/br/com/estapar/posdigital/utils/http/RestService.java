package br.com.estapar.posdigital.utils.http;

import android.content.Context;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import httpaction.HttpAction;
import httpaction.HttpCallBack;
import httpaction.HttpResponse;

import br.com.estapar.posdigital.model.enums.MediaTypes;
import br.com.estapar.posdigital.model.enums.HeaderProperty;

public class RestService {
    final HttpAction httpAction;
    final Map<String, String> headers;

    RestService(@NonNull Context context,
                @NonNull String baseUrl) {
        httpAction = new HttpAction.Builder()
                .baseUrl(baseUrl)
                .context(context)
                .build();

        // Set Http Header
        headers = new HashMap<>();
        headers.put(HeaderProperty.CONTENT_TYPE.propertyName(), MediaTypes.APPLICATION_JSON.type());
        headers.put(HeaderProperty.ACCEPT.propertyName(), MediaTypes.APPLICATION_JSON.type());
    }

    /**
     * Method to send POST to WebService
     *
     * @param uri
     * @param requestParams
     * @param playload
     * @param callBack
     */
    public void callWsPost(@NonNull String uri,
                           @NonNull Map<String, String> requestParams,
                           @NonNull JSONObject playload,
                           @NonNull final HttpCallBack callBack) {
        httpAction.sendPost(uri,
                headers,
                requestParams,
                playload,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse response) {
                        callBack.onResponse(response);
                    }

                    @Override
                    public void onError(HttpResponse response) {
                        callBack.onResponse(response);
                    }
                });
    }

    /**
     * Method to send POST to WebService
     *
     * @param uri
     * @param requestParams
     * @param callBack
     */
    public void callWsGet(@NonNull String uri,
                          @NonNull Map<String, String> requestParams,
                          @NonNull final HttpCallBack callBack) {
        httpAction.sendGet(uri,
                requestParams,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse response) {
                        callBack.onResponse(response);
                    }

                    @Override
                    public void onError(HttpResponse response) {
                        callBack.onResponse(response);
                    }
                });
    }

    /**
     * Class Builder (Design Patterns)
     */
    public static class Builder {
        private Context context;
        private String baseUrl;

        public Builder baseUrl(@NonNull String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public Builder context(@NonNull Context context) {
            this.context = context;
            return this;
        }

        public RestService build() {
            if (baseUrl == null) {
                throw new IllegalStateException("Base URL required.");
            }
            else if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            return new RestService(context, baseUrl);
        }
    }
}