package br.com.estapar.posdigital.model.mifare;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.getnet.posdigital.PosDigital;
import com.getnet.posdigital.mifare.IMifareCallback;
import com.getnet.posdigital.mifare.MifareStatus;

import br.com.estapar.posdigital.R;
import br.com.estapar.posdigital.utils.Convert;
import br.com.estapar.posdigital.utils.Utils;

public class MifareModel {
    private MifareBoxNew callback;
    private Context context;

    private double dataUser = 0;

    private static final int SECTOR_DATA      = 8;
    private static final int BLOCK_DATA_VALUE = 33;

    public static MifareModel getInstance(@NonNull Context context) {
        return new MifareModel(context);
    }

    private MifareModel(Context context) { this.context = context; }

    private String serialNo(int type) throws RemoteException {
        String result = PosDigital.getInstance().getMifare().getCardSerialNo(type);
        callback.onSerial("ID: " + result);
        return result;
    }

    /**
     * Method to new Card
     */
    protected void blankMifareCard() throws RemoteException {
        callback.open(R.string.payment_nfc_txt1);
        waitCard(new MifareCallback() {
            @Override
            public void onSuccess(int type) throws RemoteException {
                String serialCard = serialNo(type);
                if (!activate(type)) { return; }

                int contBlock = 0;
                int contSaved = 0;

                for (int sec = 0; sec < 16; sec++) {
                    boolean sectorA = authenticateSectorA(MifareType.KEY_DEFAULT, sec);
                    boolean sectorB = authenticateSectorB(MifareType.KEY_DEFAULT, sec);
                    if (sectorA || sectorB) {
                        for (int bloc = 0; bloc < 4; bloc++) {
                            // save data Key
                            if (bloc == 3) {
//                                Log.d("MIFARE","Sector: " + sec + " - Block Key: " + contBlock + " - Save Key: " + newKey);
                                boolean blocA = authenticateBlockA(MifareType.KEY_DEFAULT, contBlock);
                                boolean blocB = authenticateBlockB(MifareType.KEY_DEFAULT, contBlock);
                                if (blocA) {
                                    write(contBlock, getNewKey());
                                    contSaved += 1;
                                }
                            }
                            contBlock += 1;
                        }
                    }
                    else {
                        Log.d("MIFARE","Sector: "+sec+" not read...");
                    }
                }

                // Save Data Value
                boolean sectorA = authenticateSectorA(MifareType.KEY, SECTOR_DATA);
                boolean sectorB = authenticateSectorB(MifareType.KEY, SECTOR_DATA);
                if (sectorA || sectorB) {
                    boolean blocA = authenticateBlockA(MifareType.KEY, BLOCK_DATA_VALUE);
                    boolean blocB = authenticateBlockB(MifareType.KEY, BLOCK_DATA_VALUE);
                    if (blocA || blocB) {
                        String valueSav = "00000000000000000000000000000000";
                        if (dataUser > 0) {
                            valueSav = Utils.strZero(String.valueOf(dataUser), 16);
                            valueSav = Convert.stringToHex(valueSav);
                        }
                        write(BLOCK_DATA_VALUE, valueSav);
                    }
                }

                close();

                if (contSaved > 0) {
                    callback.onFinish("Cartão ativado com sucesso!");
                }
            }

            @Override
            public void onError(String message) { callback.onCancel(message); }
        });
    }

    /**
     * Method for increment values in Mifare Card
     *
     * @throws RemoteException
     */
    protected void incrementValue() throws RemoteException {
        callback.open(R.string.payment_nfc_txt1);
        waitCard(new MifareCallback() {
            @Override
            public void onSuccess(int type) throws RemoteException {
                serialNo(type);
                if (!activate(type)) { return; }
                boolean sectorA = authenticateSectorA(MifareType.KEY, BLOCK_DATA_VALUE);
                boolean sectorB = authenticateSectorB(MifareType.KEY, BLOCK_DATA_VALUE);
                if (sectorA || sectorB) {
                    boolean blockA = authenticateBlockA(MifareType.KEY, BLOCK_DATA_VALUE);
                    boolean blockB = authenticateBlockB(MifareType.KEY, BLOCK_DATA_VALUE);
                    if (blockA || blockB) {
                        double valueOld = MifareUtils.getDoubleValue(read(BLOCK_DATA_VALUE));
                        double valueCal = Math.round(valueOld + dataUser);
                        String valueSav = Utils.strZero(String.valueOf(valueCal), 16);
                               valueSav = Convert.stringToHex(valueSav);

                        String msg  = "Saldo anterior: R$ " + Utils.setFormatDecimal(valueOld) + "\n";
                               msg += "Valor recarga: R$ " + Utils.setFormatDecimal(dataUser) + "\n";
                               msg += "Novo saldo: R$ " + Utils.setFormatDecimal(valueCal);

                        write(BLOCK_DATA_VALUE, valueSav);
                        callback.onFinish(msg);
                    }
                }
                close();
            }

            @Override
            public void onError(String message) { callback.onCancel(message); }
        });
    }

    /**
     * Method for decrement values in Mifare Card
     *
     * @throws RemoteException
     */
    protected void decrementValue() throws RemoteException {
        callback.open(R.string.payment_nfc_txt1);
        waitCard(new MifareCallback() {
            @Override
            public void onSuccess(int type) throws RemoteException {
                serialNo(type);
                if (!activate(type)) { return; }
                boolean sectorA = authenticateSectorA(MifareType.KEY, BLOCK_DATA_VALUE);
                boolean sectorB = authenticateSectorB(MifareType.KEY, BLOCK_DATA_VALUE);
                if (sectorA || sectorB) {
                    boolean blockA = authenticateBlockA(MifareType.KEY, BLOCK_DATA_VALUE);
                    boolean blockB = authenticateBlockB(MifareType.KEY, BLOCK_DATA_VALUE);
                    if (blockA || blockB) {
                        double valueOld = MifareUtils.getDoubleValue(read(BLOCK_DATA_VALUE));
                        if (valueOld < dataUser) {
                            callback.onCancel("O Saldo (R$ "+Utils.setFormatDecimal(valueOld)+") deve ser maior que a compra!");
                        }
                        else {
                            double valueCal = Math.round(valueOld - dataUser);
                            String valueSav = Utils.strZero(String.valueOf(valueCal), 16);
                                   valueSav = Convert.stringToHex(valueSav);

                            String msg  = "Saldo anterior: R$ " + Utils.setFormatDecimal(valueOld) + "\n";
                                   msg += "Valor compra: R$ " + Utils.setFormatDecimal(dataUser) + "\n";
                                   msg += "Novo saldo: R$ " + Utils.setFormatDecimal(valueCal);

                            write(BLOCK_DATA_VALUE, valueSav);
                            callback.onFinish(msg);
                        }
                    }
                }
                close();
            }

            @Override
            public void onError(String message) { callback.onCancel(message); }
        });
    }

    /**
     * Method for read balance data in block
     *
     * @throws RemoteException
     */
    protected void balance() throws RemoteException {
        callback.open(R.string.payment_nfc_txt1);
        waitCard(new MifareCallback() {
            @Override
            public void onSuccess(int type) throws RemoteException {
                serialNo(type);
                if (!activate(type)) { return; }

                double dataVaue   = 0;
                boolean isBalence = false;
                boolean secA      = authenticateSectorA(MifareType.KEY, SECTOR_DATA);
                boolean secB      = authenticateSectorB(MifareType.KEY, SECTOR_DATA);
                if (secA || secB) {
                    boolean blocA = authenticateBlockA(MifareType.KEY, BLOCK_DATA_VALUE);
                    boolean blocB = authenticateBlockB(MifareType.KEY, BLOCK_DATA_VALUE);
                    if (blocA || blocB) {
                        dataVaue  = MifareUtils.getDoubleValue(read(BLOCK_DATA_VALUE));
                        isBalence = true;
                    }
                }
                close();
                if (isBalence) {
                    callback.onCancel("Saldo: R$ " + Utils.setFormatDecimal(dataVaue));
                }
            }

            @Override
            public void onError(String message) { callback.onCancel(message); }
        });
    }

    /**
     * Method for clear data in block
     *
     * @throws RemoteException
     */
    protected void clearBlock(int sector, int block) throws RemoteException {
        callback.open(R.string.payment_nfc_txt1);
        waitCard(new MifareCallback() {
            @Override
            public void onSuccess(int type) throws RemoteException {
                serialNo(type);
                if (!activate(type)) { return; }
                boolean sectorA = authenticateSectorA(MifareType.KEY, sector);
                boolean sectorB = authenticateSectorB(MifareType.KEY, sector);
                if (sectorA || sectorB) {
                    boolean blockA = authenticateBlockA(MifareType.KEY, block);
                    boolean blockB = authenticateBlockB(MifareType.KEY, block);
                    if (blockA || blockB) {
                        write(block, "00000000000000000000000000000000");
                    }
                }
                close();
                callback.onCancel("Retire o cartão...");
            }

            @Override
            public void onError(String message) { callback.onCancel(message); }
        });
    }

    /**
     * Method to activated Mifare Card
     *
     * @param type
     * @return
     * @throws RemoteException
     */
    private boolean activate(int type) throws RemoteException {
        boolean result = true;
        int activateResponse = PosDigital.getInstance().getMifare().activate(type);
        if (activateResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Cartão inválido...");
            result = false;
        }
        return result;
    }

    /**
     * Method for authenticate Sector A
     *
     * @param keypass
     * @param sector
     * @return
     * @throws RemoteException
     */
    private boolean authenticateSectorA(byte[] keypass, int sector) throws RemoteException {
        boolean result = true;
        int authSectorResponse = PosDigital.getInstance().getMifare().authenticateSectorWithKeyA(sector, keypass);
        if (authSectorResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Cartão inválido...");
            result = false;
        }
        return result;
    }
    private boolean authenticateSectorB(byte[] keypass, int sector) throws RemoteException {
        boolean result = true;
        int authSectorResponse = PosDigital.getInstance().getMifare().authenticateSectorWithKeyB(sector, keypass);
        if (authSectorResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Cartão inválido...");
            result = false;
        }
        return result;
    }

    /**
     * Method for aurthenticate Block A
     *
     * @param keypass
     * @param block
     * @return
     * @throws RemoteException
     */
    private boolean authenticateBlockA(byte[] keypass, int block) throws RemoteException {
        boolean result = true;
        int authBlockResponse = PosDigital.getInstance().getMifare().authenticateBlockWithKeyA(block, keypass);
        if (authBlockResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Cartão inválido...");
            result = false;
        }
        return result;
    }
    private boolean authenticateBlockB(byte[] keypass, int block) throws RemoteException {
        boolean result = true;
        int authBlockResponse = PosDigital.getInstance().getMifare().authenticateBlockWithKeyB(block, keypass);
        if (authBlockResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Cartão inválido...");
            result = false;
        }
        return result;
    }

    private String read(int block) throws RemoteException {
        String result = PosDigital.getInstance().getMifare().readBlock(block);
        return Convert.hexToString(result);
    }
    private boolean write(int block, String data) throws RemoteException {
        boolean result = true;
        int writeResponse = PosDigital.getInstance().getMifare().writeBlock(block, data);
        Log.d("MIFARE","writeResponse: " + writeResponse);
        if (writeResponse != MifareStatus.SUCCESS) {
            callback.onCancel("Não foi possível escrever no bloco");
            result = false;
        }
        return result;
    }

    /**
     * Method to wiat card
     *
     * @param mCallback
     * @throws RemoteException
     */
    private void waitCard(@NonNull MifareCallback mCallback) throws RemoteException {
        if (!PosDigital.getInstance().isInitiated()) { return; }
        PosDigital.getInstance().getMifare().searchCard(new IMifareCallback.Stub() {
            @Override
            public void onCard(int i) throws RemoteException {
                mCallback.onSuccess(i);
                beepNfc();
            }

            @Override
            public void onError(String s) throws RemoteException {
                mCallback.onError("Erro na leitura do Cartão NFC...");
            }
        });
    }

    private void beepNfc() throws RemoteException {
        int cont = 5;
        for (int jj = 0; jj < cont; jj++) {
            PosDigital.getInstance().getLed().turnOnBlue();
            PosDigital.getInstance().getLed().turnOffBlue();
        }
    }

    /**
     * Method for close Mifare
     *
     * @throws RemoteException
     */
    public void close() {
        try {
            PosDigital.getInstance().getMifare().halt();
        }
        catch (RemoteException re) {}
    }

    /**
     * Method to return new key for blank card
     *
     * @return
     */
    private String getNewKey() {
        byte[] newKey = new byte[] {
                (byte) 0xDA,
                (byte) 0xFF,
                (byte) 0xAE,
                (byte) 0x9F,
                (byte) 0x7E,
                (byte) 0x55,
                (byte) 0xFF, // Byte 06
                (byte) 0x07, // Byte 07
                (byte) 0x80, // Byte 08
                (byte) 0x69, // Byte 09 - User data
                (byte) 0xDA,
                (byte) 0xFF,
                (byte) 0xAE,
                (byte) 0x9F,
                (byte) 0x7E,
                (byte) 0x55
        };
        return Convert.bytesToHex(newKey);
    }

    /**
     * Methods Setter´s
     *
     * @param callback
     */
    public void setCallback(MifareBoxNew callback) { this.callback  = callback; }
    public void setDataUser(double dataUser)       { this.dataUser  = dataUser; }
}